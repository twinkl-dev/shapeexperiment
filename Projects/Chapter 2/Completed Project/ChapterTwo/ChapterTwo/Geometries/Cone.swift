//
//  Cone.swift
//  ARKit By Example
//
//  Created by Josh Robbins on 24/02/2018.
//  Copyright © 2018 BlackMirror. All rights reserved.
//

import Foundation
import SceneKit

class Cone: SCNNode{
    
    private var faceArray = [SCNMaterial]()
    
    /// Creates An SCNCone With Either A Single Colour Or Image For It's Material
    /// (Either A Colour Or UIImage Must Be Input)
    /// - Parameters:
    ///   - extends: Bool (If extends == false The Cone Will Taper To The Top Else It Will Extend To The Top)
    ///   - topRadius: Optional CGFloat (Defaults To 0cm)
    ///   - bottomRadius: Optional CGFloat (Defaults To 30cm)
    ///   - height: Optional CGFloat (Defaults To 60cm)
    ///   - colour: Optional UIColor
    ///   - image: Optional UIImage
    init(extends: Bool, topRadius: CGFloat = 0, bottomRadius: CGFloat = 0.3, height: CGFloat = 0.6, colour: UIColor?, image: UIImage?) {
        
        super.init()
        
        var adjustedTopRadius = topRadius
        var adjustedBottomRadius = bottomRadius
       
        if extends{
            
            adjustedTopRadius = bottomRadius
            adjustedBottomRadius = topRadius
        }
        
        self.geometry = SCNCone(topRadius: adjustedTopRadius, bottomRadius: adjustedBottomRadius, height: height)
        let material = SCNMaterial()
        
        if colour == nil{
            material.diffuse.contents = image
        }else{
            
            material.diffuse.contents = colour
        }
        
        self.geometry?.firstMaterial = material
        
    }
    
    /// Creates An SCNCone With Either A Colour Or UIImage For It's Base & Body
    /// (Either An Array [Colour] Or [UIImage] Must Be Input)
    /// - Parameters:
    ///   - extends: Bool (If extends == false The Cone Will Taper To The Top Else It Will Extend To The Top)
    ///   - topRadius: Optional CGFloat (Defaults To 0cm)
    ///   - bottomRadius: Optional CGFloat (Defaults To 30cm)
    ///   - height: Optional CGFloat (Defaults To 60cm)
    ///   - colour: Optional UIColor
    ///   - image: Optional UIImage
    init(extends: Bool, topRadius: CGFloat = 0, bottomRadius: CGFloat = 0.3, height: CGFloat = 0.6, colours: [UIColor]?, images: [UIImage]?) {
        
        super.init()
        
        var adjustedTopRadius = topRadius
        var adjustedBottomRadius = bottomRadius
        
        if extends{
            
            adjustedTopRadius = bottomRadius
            adjustedBottomRadius = topRadius
        }
        
        self.geometry = SCNCone(topRadius: adjustedTopRadius, bottomRadius: adjustedBottomRadius, height: height)
    
        var sideArray = [Any]()
        
        if colours == nil{
            guard let imageArray = images else { return }
            sideArray = imageArray
        }else{
            guard let coloursArray = colours else { return }
            sideArray = coloursArray
        }
        
        for index in 0 ..< 2{
            let face = SCNMaterial()
            face.diffuse.contents = sideArray[index]
            faceArray.append(face)
        }
        
        self.geometry?.materials = faceArray
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

//
//  Capsule.swift
//  ARKit By Example
//
//  Created by Josh Robbins on 24/02/2018.
//  Copyright © 2018 BlackMirror. All rights reserved.
//

import Foundation
import SceneKit

class Capsule: SCNNode{
    
    /// Creates An SCNCapsule With Either A Single Colour Or Image For It's Material
    /// (Either A Colour Or UIImage Must Be Input)
    /// - Parameters:
    ///   - capRadius: Optional CGFloat (Defaults To 20cm)
    ///   - height: Optional CGFloat (Defaults To 80cm)
    ///   - colour: Optional UIColor
    ///   - image: Optional UIColor
    init(capRadius: CGFloat = 0.2, height: CGFloat = 0.8, colour: UIColor?, image: UIImage?) {
        
        super.init()
        
        self.geometry = SCNCapsule(capRadius: capRadius, height: height)
        let material = SCNMaterial()
        
        if colour == nil{
            material.diffuse.contents = image
        }else{
            
            material.diffuse.contents = colour
        }
        
        self.geometry?.firstMaterial = material
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

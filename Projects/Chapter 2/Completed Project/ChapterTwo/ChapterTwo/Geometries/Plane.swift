//
//  PlaneGeometry.swift
//  ARKit By Example
//
//  Created by Josh Robbins on 18/02/2018.
//  Copyright © 2018 BlackMirror. All rights reserved.
//

import Foundation
import SceneKit

class Plane: SCNNode{
    
    /// Creates An SCNPlane With A Single Colour Or Image For It's Material
    /// (Either A Colour Or UIImage Must Be Input)
    /// - Parameters:
    ///   - width: Optional CGFloat (Defaults To 60cm)
    ///   - height: Optional CGFloat (Defaults To 30cm)
    ///   - length: Optional CGFloat (Defaults To 20cm)
    ///   - colour: Optional UIColor
    ///   - image: Optional UIColor
    ///   - doubleSided: Bool
    ///   - horizontal: The Alignment Of The Plane
    init(width: CGFloat = 0.6, height: CGFloat = 0.3, colour: UIColor?, image: UIImage?, doubleSided: Bool, horizontal: Bool) {
        
        super.init()
        
        //1. Create The Plane Geometry With Our Width & Height Parameters
        self.geometry = SCNPlane(width: width, height: height)
        
        //2. Create A New Material
        let material = SCNMaterial()
        
        //3. If A Colour Has Not Be Set The Then Material Will Be A UIImage
        if colour == nil{
           material.diffuse.contents = image
        }else{
            //The Material Will Be A UIColor
           material.diffuse.contents = colour
        }
        
        //4. Set The 1st Material Of The Plane
        self.geometry?.firstMaterial = material
        
        //5. If We Want Our Material To Be Applied On Both Sides The Set The Property To True
        if doubleSided{
            material.isDoubleSided = true
        }
      
        //6. By Default An SCNPlane Is Rendered Vertically So If We Need It Horizontal We Need To Rotate It
        if horizontal{
            self.transform = SCNMatrix4MakeRotation(-Float.pi / 2, 1, 0, 0)
        }
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //-----------------------
    //MARK: Size Calculations
    //-----------------------
    
    /// Calculates The Size & Area Of The Plane
    ///
    /// - Returns: Tuple (height: Float, width: Float, area: Float)
    func calculatedSizeOfPlane() -> (height: Float, width: Float, area: Float){
        
        let (minVector, maxVector) = self.boundingBox
        let height = maxVector.y - minVector.y
        let width = maxVector.x - minVector.x
        let area = width * height
        
        print("""
            Height Of Plane = \(height)
            Width Of Plane = \(width)
            Area Of Plane = \(area)
            """)
        
        return (height: height, width: width, area: area)
    }
}

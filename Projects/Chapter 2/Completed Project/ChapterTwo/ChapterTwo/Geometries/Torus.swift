









































//
//  Torus.swift
//  ARKit By Example
//
//  Created by Josh Robbins on 24/02/2018.
//  Copyright © 2018 BlackMirror. All rights reserved.
//

import Foundation
import SceneKit

class Torus: SCNNode{
    
    /// Creates An SCNTorus With Either A Single Colour Or Image For It's Material
    /// (Either A Colour Or UIImage Must Be Input)
    /// - Parameters:
    ///   - ringRadius: Optional CGFloat (Defaults To 30cm)
    ///   - pipeRadius: Optional CGFloat (Defaults To 10cm)
    ///   - colour: Optional UIColor
    ///   - image: Optional UIColor
    init(ringRadius: CGFloat = 0.3, pipeRadius: CGFloat = 0.1, colour: UIColor?, image: UIImage?) {
        
        super.init()
        
        self.geometry = SCNTorus(ringRadius: ringRadius, pipeRadius: pipeRadius)
       
        let material = SCNMaterial()
        
        if colour == nil{
            material.diffuse.contents = image
        }else{
            
            material.diffuse.contents = colour
        }
        
        self.geometry?.firstMaterial = material
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

//
//  Tube.swift
//  ARKit By Example
//
//  Created by Josh Robbins on 24/02/2018.
//  Copyright © 2018 BlackMirror. All rights reserved.
//

import Foundation
import SceneKit

class Tube: SCNNode{
    
    private var faceArray = [SCNMaterial]()
    
    /// Creates An SCNTube With Either A Single Colour Or Image For It's Material
    /// (Either A Colour Or UIImage Must Be Input)
    /// - Parameters:
    ///   - innerRadius: Optional CGFloat (Defaults To 10cm)
    ///   - outerRadius: Optional CGFloat (Defaults To 30cm)
    ///   - height: Optional CGFloat (Defaults To 60cm)
    ///   - colour: Optional UIColor
    ///   - image: Optional UIImage
    init(innerRadius: CGFloat = 0.1, outerRadius: CGFloat = 0.3, height: CGFloat = 0.6, colour: UIColor?, image: UIImage?) {
        
        super.init()
        
        self.geometry = SCNTube(innerRadius: innerRadius, outerRadius: outerRadius, height: height)
        
        let material = SCNMaterial()
        
        if colour == nil{
            material.diffuse.contents = image
        }else{
            
            material.diffuse.contents = colour
        }
        
        self.geometry?.firstMaterial = material
        
    }

    /// Creates An SCNTuve With Either A Colour Or UIImage For It's Base, Top And Outer & Inner Surfaces
    /// (Either An Array [Colour] Or [UIImage] Must Be Input)
    /// - Parameters:
    ///   - innerRadius: Optional CGFloat (Defaults To 10cm)
    ///   - outerRadius: Optional CGFloat (Defaults To 30cm)
    ///   - height: Optional CGFloat (Defaults To 60cm)
    ///   - colour: Optional [UIColor] - [Outer Surface, Inner Surface, Top, Bottom]
    ///   - image: Optional [UIImage] - [Outer Surface, Inner Surface, Top, Bottom]
    init(innerRadius: CGFloat = 0.1, outerRadius: CGFloat = 0.3, height: CGFloat = 0.6, colours: [UIColor]?, images: [UIImage]?) {
        
        super.init()
        
        self.geometry = SCNTube(innerRadius: innerRadius, outerRadius: outerRadius, height: height)
        
        var sideArray = [Any]()
        
        if colours == nil{
            guard let imageArray = images else { return }
            sideArray = imageArray
        }else{
            guard let coloursArray = colours else { return }
            sideArray = coloursArray
        }
        
        for index in 0 ..< 4{
            let face = SCNMaterial()
            face.diffuse.contents = sideArray[index]
            faceArray.append(face)
        }
        
        self.geometry?.materials = faceArray
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

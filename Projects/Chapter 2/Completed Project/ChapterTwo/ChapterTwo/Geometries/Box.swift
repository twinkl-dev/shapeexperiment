//
//  BoxNode.swift
//  ARKit By Example
//
//  Created by Josh Robbins on 18/02/2018.
//  Copyright © 2018 BlackMirror. All rights reserved.
//

import UIKit
import ARKit

class Box: SCNNode {
    
    private var faceArray = [SCNMaterial]()
    private let FACE_OUTLINE = "outlinedFace"
    
    /// Creates An SCNBox With A Single Colour Or Image For It's Material
    ///
    /// - Parameters:
    ///   - width: Optional CGFloat (Defaults To 20cm)
    ///   - height: Optional CGFloat (Defaults To 20cm)
    ///   - length: Optional CGFloat (Defaults To 20cm)
    ///   - colour: Optional UIColor
    ///   - image: Optional UIColor
    init(width: CGFloat = 0.2, height: CGFloat = 0.2, length: CGFloat = 0.2, colour: UIColor?, image: UIImage?) {
        
        super.init()
        
        self.geometry = SCNBox(width: width, height: height, length: length, chamferRadius: 0)
        let material = SCNMaterial()
        
        if colour == nil{
            material.diffuse.contents = image
        }else{
            
            material.diffuse.contents = colour
        }
        
        self.geometry?.firstMaterial = material
        self.rotation = SCNVector4Make(0, 1, 0, .pi / 4)
    }
    
    /// Creates An SCNBox With Either A Colour Or UIImage For Each Of It's Faces
    /// (Either An Array [Colour] Or [UIImage] Must Be Input)
    /// - Parameters:
    ///   - width: Optional CGFloat (Defaults To 20cm)
    ///   - height: Optional CGFloat (Defaults To 20cm)
    ///   - length: Optional CGFloat (Defaults To 20cm)
    ///   - colours: Optional [UIColor] - [Front, Right, Back, Left, Top, Bottom]
    ///   - images: Optional [UIImage] - [Front, Right, Back, Left, Top, Bottom]
    init(width: CGFloat = 0.2, height: CGFloat = 0.2, length: CGFloat = 0.2, colours: [UIColor]?, images: [UIImage]?) {
        
        super.init()
        
        self.geometry = SCNBox(width: width, height: height, length: length, chamferRadius: 0)
        
        var sideArray = [Any]()
        
        if colours == nil{
            guard let imageArray = images else { return }
            sideArray = imageArray
        }else{
            guard let coloursArray = colours else { return }
            sideArray = coloursArray
        }
        
        for index in 0 ..< 6{
            let face = SCNMaterial()
            face.diffuse.contents = sideArray[index]
            faceArray.append(face)
        }
        
        self.geometry?.materials = faceArray
        self.geometry?.materials = faceArray

    }
    
    /// Creates An SCNBox With A Simple Wireframe
    ///
    /// - Parameters:
    ///   - width: Optional CGFloat (Defaults To 20cm)
    ///   - height: Optional CGFloat (Defaults To 20cm)
    ///   - length: Optional CGFloat (Defaults To 20cm)
    init(width: CGFloat = 0.2, height: CGFloat = 0.2, length: CGFloat = 0.2) {
        
        super.init()
        
        self.geometry = SCNBox(width: width, height: height, length: length, chamferRadius: 0)
       
            for _ in 0 ..< 6{
                let face = SCNMaterial()
                face.diffuse.contents = UIImage(named: FACE_OUTLINE)
                face.isDoubleSided = true
                face.lightingModel = .constant
                faceArray.append(face)
            }
     
        self.geometry?.materials = faceArray
        self.rotation = SCNVector4Make(0, 1, 0, .pi / 4)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //-----------------------
    //MARK: Size Calculations
    //-----------------------
    
    /// Calculates The Size & Volume Of The Cube
    ///
    /// - Returns: Tuole - (length: Float, width: Float, height: Float, volume: Float)
    func calculatedSizeOfCube() -> (length: Float, width: Float, height: Float, volume: Float){
        
        let (minVector, maxVector) = self.boundingBox
        let length = maxVector.z - minVector.z
        let width = maxVector.x - minVector.x
        let height = maxVector.y - minVector.y
        let volume = length * width * height
        
        print("""
            Length Of Cube = \(length)
            Width Of Cube = \(width)
            Height Of Cube = \(height)
            Volume Of Cube = \(volume)
            """)
        
        return (length: length, width: width, height: height, volume: volume)
    }
}

//
//  Text.swift
//  ARKit By Example
//
//  Created by Josh Robbins on 24/02/2018.
//  Copyright © 2018 BlackMirror. All rights reserved.
//

import Foundation
import SceneKit

class Text: SCNNode{

    /// Creates An SCNText Geometry
    ///
    /// - Parameters:
    ///   - text: String (The Text To Be Displayed)
    ///   - depth: Optional CGFloat (Defaults To 1)
    ///   - font: UIFont
    ///   - textSize: Optional CGFloat (Defaults To 3)
    ///   - colour: UIColor
    init(text: String, depth: CGFloat = 1, font: String = "Helvatica", textSize: CGFloat = 3, colour: UIColor) {
        
        super.init()
        
        let textGeometry = SCNText(string: text , extrusionDepth: depth)
        textGeometry.font = UIFont(name: font, size: textSize)
        textGeometry.flatness = 0
        textGeometry.firstMaterial?.diffuse.contents = colour
        self.geometry = textGeometry
        self.scale = SCNVector3(0.01, 0.01 , 0.01)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

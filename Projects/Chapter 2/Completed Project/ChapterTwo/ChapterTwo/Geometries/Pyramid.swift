//
//  Pyramid.swift
//  ARKit By Example
//
//  Created by Josh Robbins on 24/02/2018.
//  Copyright © 2018 BlackMirror. All rights reserved.
//

import Foundation
import SceneKit

class Pyramid: SCNNode{
    
    private var faceArray = [SCNMaterial]()
    
    /// Creates An SCNPyramid With Either A Single Colour Or Image For It's Material
    /// (Either A Colour Or UIImage Must Be Input)
    /// - Parameters:
    ///   - width: Optional CGFloat (Defaults To 30cm)
    ///   - height: Optional CGFloat (Defaults To 60cm)
    ///   - length: Optional CGFloat (Defaults To 30cm)
    ///   - colour: Optional UIColor
    ///   - image: Optional UIImage
    init(width: CGFloat = 0.3, height: CGFloat = 0.6, length: CGFloat = 0.3, colour: UIColor?, image: UIImage?) {
        
        super.init()
        
        self.geometry = SCNPyramid(width: width, height: height, length: length)
        let material = SCNMaterial()
        
        if colour == nil{
            material.diffuse.contents = image
        }else{
            
            material.diffuse.contents = colour
        }
        
        self.geometry?.firstMaterial = material
        
    }
    

    /// Creates An SCNPyramid With A Different Material On Each Of Its 5 Faces
    /// (Either An Array [Colour] Or [UIImage] Must Be Input)
    /// - Parameters:
    ///   - width: Optional CGFloat (Defaults To 30cm)
    ///   - height: Optional CGFloat (Defaults To 60cm)
    ///   - length: Optional CGFloat (Defaults To 30cm)
    ///   - colour: Optional [UIColor] - [Front, Right , Left, Back, Bottom]
    ///   - image: Optional [UIImage] - [Front, Right , Left, Back, Bottom]
    init(width: CGFloat = 0.3, height: CGFloat = 0.6, length: CGFloat = 0.3, colours: [UIColor]?, images: [UIImage]?) {
        
        super.init()
        
        self.geometry = SCNPyramid(width: width, height: height, length: length)
     
        var sideArray = [Any]()
        
        if colours == nil{
            guard let imageArray = images else { return }
            sideArray = imageArray
        }else{
            guard let coloursArray = colours else { return }
            sideArray = coloursArray
        }
        
        for index in 0 ..< 5{
            let face = SCNMaterial()
            face.diffuse.contents = sideArray[index]
            faceArray.append(face)
        }
        
        self.geometry?.materials = faceArray
        
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

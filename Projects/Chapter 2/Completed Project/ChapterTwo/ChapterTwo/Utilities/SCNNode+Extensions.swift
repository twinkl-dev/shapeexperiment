//
//  SCNNode+Extensions.swift
//  ARKit By Example
//
//  Created by Josh Robbins on 18/02/2018.
//  Copyright © 2018 BlackMirror. All rights reserved.
//

import Foundation
import ARKit
import UIKit

extension SCNNode{
    
    /// Enum For Rotation Of SCNNode
    enum Rotation {
        
        case XAxis, YAxis

        var vector: SCNVector3{
            
            switch self {
            case .XAxis:
                return SCNVector3(1, 0, 0)
            case .YAxis:
                return SCNVector3(0, 1, 0)
            }
        }
    }
    
    //----------------
    //MARK: Animations
    //----------------
    
    /// Rotates The SCNNode By 360 Degrees Along Either The X Or Y Axis
    ///
    /// - Parameter rotation: Rotation
    func rotate(_ rotation: Rotation){
        
        let rotateAction = SCNAction.rotate(by: .pi * 2, around: rotation.vector, duration: 10)
        self.runAction(rotateAction)
    }
    
    /// Rotates The SCNNode By A Specified Angle Of Degrees
    /// Along Either The X Or Y Axis
    /// - Parameters:
    ///   - degrees: CGFloat
    ///   - rotation: Rotation
    ///   - animated: Bool

    func rotateBy(_ degrees: CGFloat, rotation: Rotation, animated: Bool){
        
        // Convert Degrees To Radians (1 Degree Is Equivalent To pi/180 radians)
        let rotationAsRadian = degrees * .pi/180
        
        if animated{
            
            //1a. Create An SCNAction Which Will Rotate The Object 360 Degress Around The Desired Axis Over A Course Of 10 Seconds
            let rotateAction = SCNAction.rotate(by: rotationAsRadian, around: rotation.vector, duration: 5)
            
            //1b. Run The Action
            self.runAction(rotateAction)
            
        }else{
            
            //2a. Get The X, Y, Z Values Of The Desired Rotation
            let vector3x = rotation.vector.x
            let vector3y = rotation.vector.y
            let vector3z = rotation.vector.z
            
            //2b. Set The Position & Rotation Of The Object
            self.rotation = SCNVector4Make(vector3x, vector3y, vector3z, Float(rotationAsRadian))
        }
        
    }

    
    /// Doubles The Size Of The SCNNode & Then Returns It To Its Original Size
    func growAndShrink(){
        
        let growAction = SCNAction.scale(by: 2, duration: 5)
        let shrinkAction = SCNAction.scale(by: 0.5, duration: 5)
        let animationSequence = SCNAction.sequence([growAction, shrinkAction])
        self.runAction(animationSequence)
        
    }
    
}

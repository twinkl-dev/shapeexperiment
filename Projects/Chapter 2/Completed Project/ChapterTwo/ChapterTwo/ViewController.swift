//
//  ViewController.swift
//  ARKit By Example
//
//  Created by Josh Robbins on 25/02/2018.
//  Copyright © 2018 Twinkl Limited. All rights reserved.
//

import UIKit
import ARKit

//---------------------------
//MARK: SCNNode Creation Enum
//---------------------------

/// Type Of SCNNode To Render
///
/// - Plane: SCNPlane
/// - Box: SCNBox
/// - Sphere: SCNSpere
/// - Pyramid: SCNPyramid
/// - Cone: SCNCone
/// - Cylinder: SCNCylinder
/// - Capsule: SCNCapsule
/// - Tube: SCNTube
/// - Torus: SCNTorus
enum Geometry{
    
    case Plane
    case Box
    case Sphere
    case Pyramid
    case Cone
    case Cylinder
    case Capsule
    case Tube
    case Torus
    case Text
    
}
//-----------------------
//MARK: ARSCNViewDelegate
//-----------------------

extension ViewController: ARSCNViewDelegate{
    
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) { }
    
    func renderer(_ renderer: SCNSceneRenderer, updateAtTime time: TimeInterval) {
        
        DispatchQueue.main.async {
            
            //1. Update The Tracking Status
            self.trackingStatusLabel.text = self.augmentedRealitySession.sessionStatus()
            
        }
    }
    
}

class ViewController: UIViewController {
    
    //1. Create A Reference To Our ARSCNView In Our Storyboard Which Displays The Camera Feed
    @IBOutlet weak var augmentedRealityView: ARSCNView!
    
    //2. Create A Reference To Our ARSCNView In Our Storyboard Which Will Display The ARSession Tracking Status
    @IBOutlet weak var trackingStatusLabel: UILabel!
    
    //3. Create Our ARWorld Tracking Configuration
    let configuration = ARWorldTrackingConfiguration()
    
    //4. Create Our Session
    let augmentedRealitySession = ARSession()
    
    @IBOutlet weak var geometryController: UISegmentedControl!
    
    //--------------------
    //MARK: View LifeCycle
    //--------------------
    
    override func viewDidLoad() {
        
        setupARSession()
        
        let nodeToAdd = Sphere(radius: 0.2, colour: .cyan, image: nil)
        augmentedRealityView.scene.rootNode.addChildNode(nodeToAdd)
        nodeToAdd.position = SCNVector3(0, 0, -1.5)
        
        //1. Create An SCNCylinder With A Different Colour For It's Top, Body & Base
        let node2 = Cylinder(radius: 0.2, height: 0.4, colours:  [.red, .green, .blue], images: nil)
        
        //2. Add The Cylinder To The Scene
        augmentedRealityView.scene.rootNode.addChildNode(node2)
        
        //3. Set The Cone's Position Center On The Screen & 1.5m In Front Of The Camera
        node2.position = SCNVector3(0.6, 0, -1.5)

        //1. Create An SCNPlane With A Red Texture
        let node3 = Box()
        
        //3. Set The Plane's Position Center On The Screen & 1.5m In Front Of The Camera
        node3.position = SCNVector3(1, 0, -1.5)
        
        //4. Rotate It By 270 Degrees On It's X Axis
        node3.rotateBy(270, rotation: .XAxis, animated: true)

        //augmentedRealityView.scene.rootNode.addChildNode(node3)
        
        super.viewDidLoad()
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        augmentedRealitySession.pause()
        augmentedRealitySession.run(configuration, options: runOptions(.ResetAndRemove))
        
    }
    
    
    //-----------------------
    //MARK: Geometry Creation
    //-----------------------
    
    func create(_ geometry: Geometry){
        
        var nodeToAdd: SCNNode!
        
        switch geometry {
            
        case .Plane:
            nodeToAdd = Plane(colour: .red, image: nil, doubleSided: true, horizontal: false)
        case .Box:
            nodeToAdd = Box(colours: [.red, .green, .blue, .black, .yellow, .cyan], images: nil)
        case .Sphere:
            nodeToAdd = Sphere(colour: nil, image: UIImage(named:"defaultGrid"))
        case .Pyramid:
            nodeToAdd = Pyramid(colours: [.red, .green, .blue, .black, .yellow], images: nil)
        case .Cone:
            nodeToAdd = Cone(extends: true, colours: [.red, .cyan], images: nil)
        case .Cylinder:
            nodeToAdd = Cylinder(colours: [.red, .green, .blue], images: nil)
        case .Capsule:
            nodeToAdd = Capsule(colour: nil, image: UIImage(named:"defaultGrid"))
        case .Tube:
            nodeToAdd = Tube(colours: [.red, .cyan, .black, .green], images: nil)
        case .Torus:
            nodeToAdd = Torus(colour: nil, image: UIImage(named:"defaultGrid"))
        case .Text:
            nodeToAdd = Text(text: "Hello AR", colour: .white)
        }
        
        augmentedRealityView.scene.rootNode.addChildNode(nodeToAdd)
        nodeToAdd.position = SCNVector3(0, 0, -1.5)
        nodeToAdd.rotate(.XAxis)
    }
    
    @IBAction func generateNodes(_ controller: UISegmentedControl){
        
        augmentedRealityView.scene.rootNode.enumerateChildNodes { (existingNode, _) in
            existingNode.removeFromParentNode()
        }
        
        switch controller.selectedSegmentIndex {
        case 0: create(.Plane)
        case 1: create(.Box)
        case 2: create(.Sphere)
        case 3: create(.Pyramid)
        case 4: create(.Cone)
        case 5: create(.Cylinder)
        case 6: create(.Capsule)
        case 7: create(.Tube)
        case 8: create(.Torus)
        case 9: create(.Text)
            
        default:
            create(.Text)
        }
    }
    
    //---------------
    //MARK: ARSession
    //---------------
    
    /// Sets Up The ARSession
    func setupARSession(){
        
        //1. Set The AR Session
        augmentedRealityView.session = augmentedRealitySession
        augmentedRealityView.delegate = self
        
        //2. Conifgure The Type Of Plane Detection
        configuration.planeDetection = [planeDetection(.Both)]
        
        //3. Configure The Debug Options
        augmentedRealityView.debugOptions = debug(.None)
        
        //4. If In Debug Mode Show Statistics
        #if DEBUG
        augmentedRealityView.showsStatistics = true
        #endif
        
        //5. Set The Scene Lighting
        augmentedRealityView.applyLighting()
        
        //6. Run The Session
        augmentedRealitySession.run(configuration, options: runOptions(.ResetAndRemove))
        
    }
    
}


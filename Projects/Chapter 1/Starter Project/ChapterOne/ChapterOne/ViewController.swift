//
//  ViewController.swift
//  ARKit By Example
//
//  Created by Josh Robbins on 17/02/2018.
//  Copyright © 2018 BlackMirror. All rights reserved.
//

import UIKit
import ARKit

class ViewController: UIViewController {

    //--------------------
    //MARK: View LifeCycle
    //--------------------
  
    override func viewDidLoad() {
        
        super.viewDidLoad()
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
     
    }

    override func viewDidAppear(_ animated: Bool) {
        
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
      

    }
   

}


//
//  Capsule.swift
//
//
//  Created by Josh Robbins on 24/02/2018.
//  Copyright © 2018 BlackMirror. All rights reserved.
//

import Foundation
import SceneKit

class Capsule: SCNNode{
    
    /// Creates An SCNCapsule With Either A Single Colour Or Image For It's Material
    /// (Either A Colour Or UIImage Must Be Input)
    /// - Parameters:
    ///   - capRadius: Optional CGFloat (Defaults To 20cm)
    ///   - height: Optional CGFloat (Defaults To 80cm)
    ///   - content: Any (UIColor Or UIImage)
    init(capRadius: CGFloat = 0.2, height: CGFloat = 0.8, content: Any) {
        
        super.init()
        
        //1. Create The Capsule's Geometry With Our Radius Parameter
        self.geometry = SCNCapsule(capRadius: capRadius, height: height)
        
        //2. Create A New Material
        let material = SCNMaterial()
        
        if let colour = content as? UIColor{
            
            //The Material Will Be A UIColor
            material.diffuse.contents = colour
            
        }else if let image = content as? UIImage{
            
            //The Material Will Be A UIImage
            material.diffuse.contents = image
            
        }else{
            
            //Set Our Material Colour To Cyan
            material.diffuse.contents = UIColor.cyan
            
        }
        
        //3. Set The Material Of The Capsule
        self.geometry?.firstMaterial = material
    }
    
    required init?(coder aDecoder: NSCoder) { fatalError("Capsule Node Coder Not Implemented") }
    
}

//
//  Tube.swift
//  Setup
//
//  Created by Josh Robbins on 24/02/2018.
//  Copyright © 2018 BlackMirror. All rights reserved.
//

import Foundation
import SceneKit

class Tube: SCNNode{
    
    private var faceArray = [SCNMaterial]()
    
    /// Creates An SCNTube With Either A Single Colour Or Image For It's Material
    /// (Either A Colour Or UIImage Must Be Input)
    /// - Parameters:
    ///   - innerRadius: Optional CGFloat (Defaults To 10cm)
    ///   - outerRadius: Optional CGFloat (Defaults To 30cm)
    ///   - height: Optional CGFloat (Defaults To 60cm)
    ///   - colour: Optional UIColor
    ///   - image: Optional UIImage
    init(innerRadius: CGFloat = 0.1, outerRadius: CGFloat = 0.3, height: CGFloat = 0.6, colour: UIColor?, image: UIImage?) {
        
        super.init()
       
        //1. Create The Tubes Geometry With Our Radius Parameter
        self.geometry = SCNTube(innerRadius: innerRadius, outerRadius: outerRadius, height: height)
        
        //2. Create A New Material
        let material = SCNMaterial()
        
        //3. If A Colour Has Not Be Set The Then Material Will Be A UIImage
        if colour == nil{
            material.diffuse.contents = image
        }else{
            //The Material Will Be A UIColor
            material.diffuse.contents = colour
        }
        
        //4. Set The Material Of The Tube
        self.geometry?.firstMaterial = material
        
    }

    /// Creates An SCNTube With Either A Colour Or UIImage For It's Base, Top And Outer & Inner Surfaces
    /// (Either An Array [Colour] Or [UIImage] Must Be Input)
    /// - Parameters:
    ///   - innerRadius: Optional CGFloat (Defaults To 10cm)
    ///   - outerRadius: Optional CGFloat (Defaults To 30cm)
    ///   - height: Optional CGFloat (Defaults To 60cm)
    ///   - colour: Optional [UIColor] - [Outer Surface, Inner Surface, Top, Bottom]
    ///   - image: Optional [UIImage] - [Outer Surface, Inner Surface, Top, Bottom]
    init(innerRadius: CGFloat = 0.1, outerRadius: CGFloat = 0.3, height: CGFloat = 0.6, colours: [UIColor]?, images: [UIImage]?) {
        
        super.init()
  
        //1. Create The Tube  Geometry With Our Inner & Outer Radiuses As Well As Our Height
        self.geometry = SCNTube(innerRadius: innerRadius, outerRadius: outerRadius, height: height)
        
        //2. Create A Temporary Array To Store Either Our UIColors Or UIImages
        var sideArray = [Any]()
        
        //3. If Our Color Array Is Nil Then Our Side Array Will Be Equal To Our Images Array
        if colours == nil{
            guard let imageArray = images else { return }
            sideArray = imageArray
        }else{
            //Our Side Array Will Be Equal To Our Colours Array
            guard let coloursArray = colours else { return }
            sideArray = coloursArray
        }
        
        //4. Loop Through The Number Of Faces & Create A New Material For Each
        for index in 0 ..< 4{
            let face = SCNMaterial()
            face.diffuse.contents = sideArray[index]
            //Add The Material To Our Face Array
            faceArray.append(face)
        }
        
        //5. Set The Tubes Materials From Our Face Array
        self.geometry?.materials = faceArray
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

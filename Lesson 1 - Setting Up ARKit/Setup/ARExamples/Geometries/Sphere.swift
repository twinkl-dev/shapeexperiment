//
//  SCNGeometries.swift
//  Setup
//
//  Created by Josh Robbins on 24/02/2018.
//  Copyright © 2018 BlackMirror. All rights reserved.
//

import Foundation
import SceneKit

class Sphere: SCNNode{

/// Creates An SCNSphere With Either A Single Colour Or Image For It's Material
/// (Either A Colour Or UIImage Must Be Input)
/// - Parameters:
///  - radius: Optional CGFloat (Defaults To 30cm)
/// - content: Any (UIColor Or UIImage)
init(radius: CGFloat = 0.3, content: Any) {
    
    super.init()
    
    //1. Create The Sphere Geometry With Our Radius Parameter
    self.geometry = SCNSphere(radius: radius)
    
    //2. Create A New Material
    let material = SCNMaterial()
    
    //3. If A Colour Has Not Be Set The Then Material Will Be A UIImage
    if let colour = content as? UIColor{
        
        //The Material Will Be A UIColor
        material.diffuse.contents = colour
        
    }else if let image = content as? UIImage{
        
        //The Material Will Be A UIImage
        material.diffuse.contents = image
        
    }else{
        
        //Set Our Material Colour To Cyan
        material.diffuse.contents = UIColor.cyan
        
    }
    
    //4. Set The Material Of The Sphere
    self.geometry?.firstMaterial = material
    
}

required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
}

}


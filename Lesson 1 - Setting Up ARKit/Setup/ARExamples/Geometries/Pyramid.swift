//
//  Pyramid.swift
//  Setup
//
//  Created by Josh Robbins on 24/02/2018.
//  Copyright © 2018 BlackMirror. All rights reserved.
//

import Foundation
import SceneKit

class Pyramid: SCNNode{

private var faceArray = [SCNMaterial]()

/// Creates An SCNPyramid With Either A Single Colour Or Image For It's Material
/// (Either A Colour Or UIImage Must Be Input)
/// - Parameters:
///   - width: Optional CGFloat (Defaults To 30cm)
///   - height: Optional CGFloat (Defaults To 60cm)
///   - length: Optional CGFloat (Defaults To 30cm)
///   - content: Any (UIColor Or UIImage)
init(width: CGFloat = 0.3, height: CGFloat = 0.6, length: CGFloat = 0.3, content: Any) {

super.init()

//1. Create The Pyramid Geometry With Our Width, Height & Length Parameters
self.geometry = SCNPyramid(width: width, height: height, length: length)
    
//2. Create A New Material
let material = SCNMaterial()

//3. Set The Material Contents
if let colour = content as? UIColor{
    
    //The Material Will Be A UIColor
    material.diffuse.contents = colour
    
}else if let image = content as? UIImage{
    
    //The Material Will Be A UIImage
    material.diffuse.contents = image
    
}else{
    
    //Set Our Material Colour To Cyan
    material.diffuse.contents = UIColor.cyan
    
}

//4. Set The Material Of The Pyramid
self.geometry?.firstMaterial = material

}


/// Creates An SCNPyramid With A Different Material On Each Of Its 5 Faces
/// (Either An Array [Colour] Or [UIImage] Must Be Input)
/// - Parameters:
///   - width: Optional CGFloat (Defaults To 30cm)
///   - height: Optional CGFloat (Defaults To 60cm)
///   - length: Optional CGFloat (Defaults To 30cm)
///   - contents: [Any] - [Front, Right , Left, Back, Bottom]
init(width: CGFloat = 0.3, height: CGFloat = 0.6, length: CGFloat = 0.3, contents: [Any]) {
    
    super.init()
    
    //1. Create The Pyramid Geometry With Our Width, Height & Length Parameters
    self.geometry = SCNPyramid(width: width, height: height, length: length)
    
    //2. Create A Temporary Array To Store Either Our UIColors Or UIImages
    var sideArray = [Any]()
    
    //3. Assign The Colours Or UIImages
    if let colours = contents as? [UIColor], colours.count == 5{
        
        //The Materials Will Be A UIColor
        sideArray = colours
        
    }else if let images = contents as? [UIImage], images.count == 5{
        
        //The Materials Will Be A UIImage
        sideArray = images
        
    }else{
        
        //Set Our Material Colours To Red, Orange, Green, Blue, Purple
        sideArray = [UIColor.red, UIColor.orange, UIColor.green, UIColor.blue, UIColor.purple]
        
    }
    //5. Loop Through The Number Of Faces & Create A New Material For Each
    for index in 0 ..< 5{
        let face = SCNMaterial()
        face.diffuse.contents = sideArray[index]
        //Add The Material To Our Face Array
        faceArray.append(face)
    }
    
    //6. Set The Pyramids Materials From Our Face Array
    self.geometry?.materials = faceArray
    
}

required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
}

}

//class Test: SCNNode {
//
///// Adds A Custom Pyramid To The Scene Hieracy With A Single Colour For It's Material
//func addSingleColouredPyramid(){
//    
//    //1. Create Our Pyramid Node With A Single Colour
//    let singleColouredCone = Cone(extends: false, content: UIColor.cyan)
//    
//    //2. Set It's Position 1.5m Away From The Camera
//    singleColouredCone.position = SCNVector3(0, 0, -1.5)
//    
//    
//    //self.
//    //3. Add Our Cone To Our ARSCNView
//    self.augmentedRealityView.scene.rootNode.addChildNode(singleColouredCone)
//   
//
//}
//
///// Adds A Custom Cone To The Scene Hieracy With A Different Colour For It's Base & Body
//func addMultiColouredPyramid(){
//    
//    //1. Create Our Cone Node With A Different Colour For It's Base & Body
//    let multiColouredCone = Cone(extends: true, contents: [UIColor.cyan])
//    
//    //2. Set It's Position 1.5m Away From The Camera
//    multiColouredCone.position = SCNVector3(0, 0, -1.5)
//    
//    //3. Add Our Cone To Our ARSCNView
//    self.augmentedRealityView.scene.rootNode.addChildNode(multiColouredCone)
//    
//}
//}

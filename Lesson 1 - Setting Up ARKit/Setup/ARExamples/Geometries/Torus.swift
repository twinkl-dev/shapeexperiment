//
//  Torus.swift
//
//
//  Created by Josh Robbins on 24/02/2018.
//  Copyright © 2018 BlackMirror. All rights reserved.
//

import Foundation
import SceneKit

class Torus: SCNNode{
    
    /// Creates An SCNTorus With Either A Single Colour Or Image For It's Material
    /// (Either A Colour Or UIImage Must Be Input)
    /// - Parameters:
    ///   - ringRadius: Optional CGFloat (Defaults To 30cm)
    ///   - pipeRadius: Optional CGFloat (Defaults To 10cm)
    ///   - content: Any (UIColor Or UIImage)
    init(ringRadius: CGFloat = 0.3, pipeRadius: CGFloat = 0.1, content: Any) {
        
        super.init()
        
        //1. Create The Torus's Geometry With Our Radius Parameter
        self.geometry = SCNTorus(ringRadius: ringRadius, pipeRadius: pipeRadius)
       
        //2. Create A New Material
        let material = SCNMaterial()
        
        //3. Set The Material Contents
        if let colour = content as? UIColor{
            
            //The Material Will Be A UIColor
            material.diffuse.contents = colour
            
        }else if let image = content as? UIImage{
            
            //The Material Will Be A UIImage
            material.diffuse.contents = image
            
        }else{
            
            //Set Our Material Colour To Cyan
            material.diffuse.contents = UIColor.cyan
            
        }
        
         //4. Set The Material Of The Torus
        self.geometry?.firstMaterial = material
        
    }
    
    required init?(coder aDecoder: NSCoder) { fatalError("Torus Node Coder Not Implemented") }
    
}

//
//  Cylinder.swift
//
//
//  Created by Josh Robbins on 24/02/2018.
//  Copyright © 2018 BlackMirror. All rights reserved.
//

import Foundation
import SceneKit

class Cylinder: SCNNode{
    
    private var faceArray = [SCNMaterial]()
    
    /// Creates An SCNCylinder With Either A Single Colour Or Image For It's Material
    /// (Either A Colour Or UIImage Must Be Input)
    /// - Parameters:
    ///   - radius: Optional CGFloat (Defaults To 30cm)
    ///   - height: Optional CGFloat (Defaults To 60cm)
    ///   - content: Any (UIColor Or UIImage)
    init(radius: CGFloat = 0.3, height: CGFloat = 0.6, content: Any) {
       
        super.init()
        
        //1. Create The Cylinder Geometry With Our Radius & Height Parameters
        self.geometry = SCNCylinder(radius: radius, height: height)
        
        //2. Create A New Material
        let material = SCNMaterial()
        
        //3. Set The Material Contents
        if let colour = content as? UIColor{
            
            //The Material Will Be A UIColor
            material.diffuse.contents = colour
            
        }else if let image = content as? UIImage{
            
            //The Material Will Be A UIImage
            material.diffuse.contents = image
            
        }else{
            
            //Set Our Material Colour To Cyan
            material.diffuse.contents = UIColor.cyan
            
        }
        
        //4. Set The Material Of The Cylinder
        self.geometry?.firstMaterial = material
        
    }
    
    
    /// Creates An SCNCylinder With Either A Colour Or UIImage For It's Base, Top & Body
    /// (Either An Array [Colour] Or [UIImage] Must Be Input)
    /// - Parameters:
    ///   - radius: Optional CGFloat (Defaults To 30cm)
    ///   - height: Optional CGFloat (Defaults To 60cm)
    ///   - contents: Optional [Any] - [Body, Top, Bottom]
    init(radius: CGFloat = 0.3, height: CGFloat = 0.6, contents: [Any]) {
        
        super.init()
        
        //1. Create The Cylinder Geometry With Our Radius & Height Parameters
        self.geometry = SCNCylinder(radius: radius, height: height)
        
        //2. Create A Temporary Array To Store Either Our UIColors Or UIImages
        var sideArray = [Any]()
        
        //3. Assign The Colours Or UIImages
        if let colours = contents as? [UIColor], colours.count == 3{
            
            //The Materials Will Be A UIColor
            sideArray = colours
            
        }else if let images = contents as? [UIImage], images.count == 3{
            
            //The Materials Will Be A UIImage
            sideArray = images
            
        }else{
            
            //Set Our Material Colours To Cyan, Red, Green
            sideArray = [UIColor.cyan, UIColor.red, UIColor.green]
            
        }
        
        //4. Loop Through The Number Of Faces & Create A New Material For Each
        for index in 0 ..< 3{
            let face = SCNMaterial()
            face.diffuse.contents = sideArray[index]
            
            //Add The Material To Our Face Array
            faceArray.append(face)
        }
        
        //6. Set The Cylinders Materials From Our Face Array
        self.geometry?.materials = faceArray
        
    }
    
    required init?(coder aDecoder: NSCoder) { fatalError("Cylinder Node Coder Not Implemented") }
    
}

//
//  VideoNode.swift
//  Setup
//
//  Created by Josh Robbins on 02/03/2018.
//  Copyright © 2018 BlackMirror. All rights reserved.
//

import Foundation
import SceneKit
import AVFoundation
import SpriteKit

class VideoNode: SCNNode{
    
    var spriteKitScene: SKScene!
    var videoPlayerNode: SKVideoNode!
    var videoPlayer: AVPlayer!
    
    /// Creates An SCNPlane With A Single Colour Or Image For It's Material
    /// (Either A Colour Or UIImage Must Be Input)
    /// - Parameters:
    ///   - width: Optional CGFloat (Defaults To 60cm)
    ///   - height: Optional CGFloat (Defaults To 30cm)
    ///   - length: Optional CGFloat (Defaults To 20cm)
    ///   - colour: Optional UIColor
    ///   - image: Optional UIColor
    ///   - doubleSided: Bool
    ///   - horizontal: The Alignment Of The Plane
    init(width: CGFloat = 0.6, height: CGFloat = 0.3, videoPath: String, doubleSided: Bool, horizontal: Bool) {
        
        super.init()
        
        //1. Create The Plane Geometry With Our Width & Height Parameters
        self.geometry = SCNPlane(width: width, height: height)
        
        //2.
        
        let url = URL(fileURLWithPath: videoPath)
        videoPlayer = AVPlayer(url: url as URL)
        videoPlayerNode = SKVideoNode(avPlayer: videoPlayer)
        videoPlayerNode.yScale = -1
        
        spriteKitScene = SKScene(size: CGSize(width: 600, height: 300))
        spriteKitScene.scaleMode = .aspectFit
        videoPlayerNode.position = CGPoint(x: spriteKitScene.size.width/2, y: spriteKitScene.size.height/2)
        videoPlayerNode.size = spriteKitScene.size
        spriteKitScene.addChild(videoPlayerNode)
        
     
        self.geometry?.firstMaterial?.diffuse.contents = spriteKitScene

        //5. If We Want Our Material To Be Applied On Both Sides The Set The Property To True
        if doubleSided{
            self.geometry?.firstMaterial?.isDoubleSided = true
        }
        
        //6. By Default An SCNPlane Is Rendered Vertically So If We Need It Horizontal We Need To Rotate It
        if horizontal{
            self.transform = SCNMatrix4MakeRotation(-Float.pi / 2, 1, 0, 0)
        }
        
        videoPlayer.play()
        videoPlayer.volume = 1
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
 
}

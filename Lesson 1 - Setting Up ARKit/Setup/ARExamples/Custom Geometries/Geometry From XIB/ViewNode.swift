//
//  ViewNode.swift
//  Setup
//
//  Created by Josh Robbins on 01/03/2018.
//  Copyright © 2018 BlackMirror. All rights reserved.
//

import Foundation
import SceneKit

class ViewNode: SCNNode{
    
    /// Creates An SCNPlane With A Single Colour Or Image For It's Material
    /// (Either A Colour Or UIImage Must Be Input)
    /// - Parameters:
    ///   - width: Optional CGFloat (Defaults To 60cm)
    ///   - height: Optional CGFloat (Defaults To 30cm)
    ///   - length: Optional CGFloat (Defaults To 20cm)
    ///   - colour: Optional UIColor
    ///   - image: Optional UIColor
    ///   - doubleSided: Bool
    ///   - horizontal: The Alignment Of The Plane
    init(width: CGFloat = 0.6, height: CGFloat = 0.3) {
        
        super.init()
    
        //1. Create The Plane Geometry With Our Width & Height Parameters
        self.geometry = SCNPlane(width: width, height: height)
        
        //2. Create A New Material
        let material = SCNMaterial()
        
        //3. Load The Custom XIB
        let contentView = UINib(nibName: "ViewNode", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! ViewNodeController
        material.diffuse.contents = contentView
        material.isDoubleSided = false
        
        self.geometry?.firstMaterial = material
        //self.geometry?.firstMaterial?.transparencyMode = SCNTransparencyMode.rgbZero
        //self.geometry?.firstMaterial?.blendMode = .max
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

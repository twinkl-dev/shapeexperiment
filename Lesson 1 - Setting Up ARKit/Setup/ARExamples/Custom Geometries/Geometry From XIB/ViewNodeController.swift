//
//  ViewNodeController.swift
//  Setup
//
//  Created by Josh Robbins on 01/03/2018.
//  Copyright © 2018 BlackMirror. All rights reserved.
//

import UIKit

class ViewNodeController: UIView {

    @IBOutlet var menuButtons: [UIButton]!
    
    //----------------
    //MARK: Life Cycle
    //----------------
    override init(frame: CGRect) {
    
      super.init(frame: frame)
    
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
      
    }
    
    //We Call This So Out IBOutletCollection Will Be Loaded
    override func awakeFromNib() {
        
        //1. Assign A Unique Tag To The Menu Buttons
        for index in 0 ..< menuButtons.count{
            
            let menuButton = menuButtons[index]
            menuButton.tag = index
            menuButton.addTarget(self, action:  #selector(menuButtonPressed(_:)), for: .touchUpInside)
        }
        
    }
  
    //-----------------
    //MARK: Interaction
    //-----------------
    
    /// Changes The Background Colour Of The Button Pressed
    ///
    /// - Parameter sender: UIButton
    @objc func menuButtonPressed(_ sender: UIButton){
    
        //1. Change The Colour Of The Button
        switch sender.tag {
            
        case 0:
            sender.backgroundColor = .red
        case 1:
            sender.backgroundColor = .green
        case 2:
            sender.backgroundColor = .yellow
        default:
            sender.backgroundColor = .clear
        }
        
        //2. Reset The Colour By Fading It Out
        UIView.animate(withDuration: 2) {
            
            sender.backgroundColor = .clear
        }
    }
    
}

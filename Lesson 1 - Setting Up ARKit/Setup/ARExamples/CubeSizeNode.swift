//
//  CubeSizeNode.swift
//  Setup
//
//  Created by Josh Robbins on 22/02/2018.
//  Copyright © 2018 BlackMirror. All rights reserved.
//

import UIKit
import ARKit

class CubeSizeNode: SCNNode {
    
    private var lengthLabel: SCNText?
    private var widthLabel: SCNText?
    private var heightLabel: SCNText?
    
    
    init(width: CGFloat = 0.3, height: CGFloat = 0.3, length: CGFloat = 0.3) {
        
        super.init()
        
        guard let model = SCNScene(named: "ARModels.scnassets/Cube.scn"),
              let cube = model.rootNode.childNode(withName: "Cube", recursively: false),
              let labelWidthHolder = cube.childNode(withName: "lengthLabel", recursively: false)?.geometry as? SCNText,
              let labelHeightHolder = cube.childNode(withName: "widthLabel", recursively: false)?.geometry as? SCNText,
              let labelLengthHolder = cube.childNode(withName: "heightLabel", recursively: false)?.geometry as? SCNText
              else{ return }
        
        cube.scale = SCNVector3(width, height, length)
        
        self.lengthLabel = labelLengthHolder
        self.lengthLabel?.string = "0.3m"
        self.widthLabel = labelWidthHolder
        self.widthLabel?.string = "0.3m"
        self.heightLabel = labelHeightHolder
        self.heightLabel?.string = "0.3m"
        
        self.addChildNode(cube)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

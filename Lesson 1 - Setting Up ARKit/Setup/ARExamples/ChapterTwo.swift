//
//  ChapterTwo.swift
//  Setup
//
//  Created by Josh Robbins on 24/02/2018.
//  Copyright © 2018 BlackMirror. All rights reserved.
//

import UIKit
import ARKit
import CoreMotion
import WebKit

//---------------------------
//MARK: SCNNode Creation Enum
//---------------------------

//-----------------------
//MARK: SCNNode Extension
//-----------------------

extension ChapterTwo: WKNavigationDelegate{
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
        
        
    }
}

extension SCNNode{
    
    /// Creates A Pulsing Animation On An Infinite Loop
    ///
    /// - Parameter duration: TimeInterval
    func highlightNodeWithDurarion(_ duration: TimeInterval){
        
        //1. Create An SCNAction Which Emmits A Red Colour Over The Passed Duration Parameter
        let highlightAction = SCNAction.customAction(duration: duration) { (node, elapsedTime) in
            
            let color = UIColor(red: elapsedTime/CGFloat(duration), green: 0, blue: 0, alpha: 1)
            let currentMaterial = self.geometry?.firstMaterial
            currentMaterial?.emission.contents = color
            
        }
        
        //2. Create An SCNAction Which Removes The Red Emissio Colour Over The Passed Duration Parameter
        let unHighlightAction = SCNAction.customAction(duration: duration) { (node, elapsedTime) in
            let color = UIColor(red: CGFloat(1) - elapsedTime/CGFloat(duration), green: 0, blue: 0, alpha: 1)
            let currentMaterial = self.geometry?.firstMaterial
            currentMaterial?.emission.contents = color
            
        }
        
        //3. Create An SCNAction Sequence Which Runs The Actions
        let pulseSequence = SCNAction.sequence([highlightAction, unHighlightAction])
        
        //4. Set The Loop As Infinitie
        let infiniteLoop = SCNAction.repeatForever(pulseSequence)
        
        //5. Run The Action
        self.runAction(infiniteLoop)
    }
    
}

extension  CGFloat{
    
    /// Converts Degrees To Radians
    var degreesToRadians:CGFloat { return CGFloat(self) * .pi/180}
    
}

/// Type Of SCNNode To Render
///
/// - Plane: SCNPlane
/// - Box: SCNBox
/// - Sphere: SCNSpere
/// - Pyramid: SCNPyramid
/// - Cone: SCNCone
/// - Cylinder: SCNCylinder
/// - Capsule: SCNCapsule
/// - Tube: SCNTube
/// - Torus: SCNTorus
enum Geometry: Int{
    
    case Plane
    case Box
    case Sphere
    case Pyramid
    case Cone
    case Cylinder
    case Capsule
    case Tube
    case Torus
    case Text
    case ViewNode
    case VideoNode
    
    static let count = [Plane, Box, Sphere, Pyramid, Cone, Cylinder, Capsule, Tube, Torus, Text].count
    
}


//-----------------------
//MARK: ARSCNViewDelegate
//-----------------------

extension ChapterTwo: ARSCNViewDelegate{
    
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        //
        //        node.name = String(nodeID)
        //
        //        anchorsAdded[anchor] = node
        //
        //        nodeID += 1
        //
        //        print("Node ID = \(node.name!) For Anchor == \(anchor.identifier)")
        //
        //        //1. Get The Current ARPlaneAnchor
        //        guard let anchor = anchor as? ARPlaneAnchor else { return }
        //
        //        //2. Create The PlaneNode
        ////        if planeNode == nil{
        ////            planeNode = PlaneNode(anchor: anchor, node: node, image: true, identifier: 0, opacity: 1)
        ////            node.addChildNode(planeNode!)
        ////            planeNode?.name = String("Detected Plane")
        ////        }
        //
        //        planeNode = PlaneNode(anchor: anchor, node: node, image: true, identifier: 0, opacity: 1)
        //        node.addChildNode(planeNode!)
        //        planeNode?.name = String("Detected Plane")
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        
        //guard let anchor = anchor as? ARPlaneAnchor, let existingPlane = planeNode else { return }
        //existingPlane.update(anchor)
        
        
    }
    
    func renderer(_ renderer: SCNSceneRenderer, updateAtTime time: TimeInterval) {
        
        
        
        
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didRenderScene scene: SCNScene, atTime time: TimeInterval) {
        
        //        /*
        //        Get The Pitch (Rotation Around X Axis)
        //        Get The Yaw (Rotation Around Y Axis)
        //        Get The Roll (Rotation Around Z Axis)
        //        */
        //        guard let pitch = self.augmentedRealityView.session.currentFrame?.camera.eulerAngles.x,
        //              let yaw =   self.augmentedRealityView.session.currentFrame?.camera.eulerAngles.y,
        //              let roll = self.augmentedRealityView.session.currentFrame?.camera.eulerAngles.z else { return }
        //
        //        print("""
        //         Pitch = \(degreesFrom(pitch))
        //         Yaw = \(degreesFrom(yaw))
        //         Roll = \(degreesFrom(roll))
        //        """)
        //
        //        let newRotation = SCNVector3Make(pitch, yawn, roll)
        //        whateverNode.eulerAngles = newRotation
    }
    
    
    /// Convert Radians To Degrees
    ///
    /// - Parameter radian: Float
    /// - Returns: Float
    func degreesFrom( _ radian: Float) -> Float{
        
        return radian * Float(180.0 / Double.pi)
        
    }
}

enum BoxFaces: Int{
    
    case Front, Right, Back, Left, Top, Bottom
}


class ChapterTwo: UIViewController {
    
    var anchorsAdded = [ARAnchor: SCNNode]()
    var nodeID: Int = 0
    
    //1. Create A Reference To Our ARSCNView In Our Storyboard Which Displays The Camera Feed
    @IBOutlet weak var augmentedRealityView: ARSCNView!
    
    //2. Create A Reference To Our ARSCNView In Our Storyboard Which Will Display The ARSession Tracking Status
    @IBOutlet weak var trackingStatusLabel: UILabel!
    
    //3. Create Our ARWorld Tracking Configuration
    let configuration = ARWorldTrackingConfiguration()
    
    //4. Create Our Session
    let augmentedRealitySession = ARSession()
    
    @IBOutlet weak var geometryController: UISegmentedControl!
    
    var planesDetected = [ARPlaneAnchor: SCNNode]()
    var planeID: Int = 0
    var planeNode:PlaneNode?
    var currentNode: SCNNode?
    var originalPosition: SCNVector3?
    var nodeToAdd: SCNNode!
    
    /// Detects If An Object Is In View Of The Camera Frostrum
    func detectNodeInFrostrumOfCamera(){
        
        //1. Get The Current Point Of View
        if let currentPointOfView = augmentedRealityView.pointOfView{
            
            //2. Loop Through All The Detected Planes
            for anchor in anchorsAdded{
                
                let node = anchor.value
                
                if augmentedRealityView.isNode(node, insideFrustumOf: currentPointOfView){
                    
                    print("Node With ID \(node.name!) Is In View")
                    
                }else{
                    
                    print("Node With ID \(node.name!) Is Not In View")
                }
                
            }
            
        }
    }
    //--------------------
    //MARK: View LifeCycle
    //--------------------
    
    
    
    @IBOutlet var outputImageView:UIImageView!
    
    private var shapeTypeToPlace : ShapeType = .cuboid
   
    // Just for Testing:
    private var shapeTypeIndex = 0
    private let shapeTypeList : [ShapeType] = [.cuboid, .squarePyramid, .cone, .sphere, .triangularPrism, .cylinder]
    private var isPlacing: Bool = true {
        didSet{
            trackingStatusLabel.text = isPlacing ? "Placing" : "Can Touch shapes"
        }
    }
    
    @IBAction func cycleShapes(_ sender: UIButton) {
        
        if(shapeTypeIndex < shapeTypeList.count)
        {
            shapeTypeIndex += 1
        }
        else
        {
            shapeTypeIndex = 0
        }
        
        if(shapeTypeIndex != shapeTypeList.count)
        {
            shapeTypeToPlace = shapeTypeList[shapeTypeIndex]
        }
        trackingStatusLabel.text = "Current shape is \(shapeTypeToPlace.rawValue)"
    }
    
    @IBAction func placingPressed(_ sender: UIButton) {
        isPlacing = !isPlacing
    }
    override func viewDidLoad() {
        
        setupARSession()
        
        //createPlane()
        
        //self.augmentedRealityView.scene.rootNode.addChildNode(ShapeBuilder.generateShape(shapeType: .cuboid))
        //self.augmentedRealityView.scene.rootNode.addChildNode(ShapeBuilder.generateShape(shapeType: .cone))
        //self.augmentedRealityView.scene.rootNode.addChildNode(ShapeBuilder.generateShape(shapeType: .sphere))
        //self.augmentedRealityView.scene.rootNode.addChildNode(ShapeBuilder.generateShape(shapeType: .triangularPrism))
        //self.augmentedRealityView.scene.rootNode.addChildNode(ShapeBuilder.generateShape(shapeType: .cylinder))
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap(rec:)))
        
        //Add recognizer to sceneview
        augmentedRealityView.addGestureRecognizer(tap)
        
        super.viewDidLoad()
        
        
        
        //        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(swipeToSelectGeometry(_:)))
        //        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        //
        //        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(swipeToSelectGeometry(_:)))
        //        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        //
        //        self.view.addGestureRecognizer(swipeLeft)
        //        self.view.addGestureRecognizer(swipeRight)
        
        // createNodes()
        
        //createTrueOrFalseMenuWithText()
        //addMultiColouredCone()
    }
    
    
    
    @objc func handleTap(rec: UITapGestureRecognizer){
        
       
        if(!isPlacing) {
            if rec.state == .ended {
            let location: CGPoint = rec.location(in: augmentedRealityView)
            let hits = augmentedRealityView.hitTest(location, options: [SCNHitTestOption.searchMode : 1])
            if !hits.isEmpty {
                let tappedNode = hits.first?.node
                if let tappedNode = hits.first?.node{
                    if let faceIndex = BoxFaces(rawValue:  hits.first!.geometryIndex) {
                        trackingStatusLabel.text = "User Has Hit \(faceIndex)"
                        //print("User Has Hit \(faceIndex)")
                    }
                }
            }
        }
    }

    }
    
    @objc func detectNode(_ sender: UITapGestureRecognizer) {
        // let sceneView = sender.view as! ARSCNView
        let location = sender.location(in: augmentedRealityView)
        let results = augmentedRealityView.hitTest(location, options: [SCNHitTestOption.searchMode : 1])
        
        for result in results.filter( { $0.node.name != nil }) {
            print(result.node.name)
        }
    }
    
    /// Adds A Custom Capsule To The Scene Hieracy With A Single Colour For It's Material
    func addCapsuleWithColour(){
        
        //1. Create Our Capsule With A Single Colour
        let capsule = Capsule(content: UIColor.cyan)
        
        //2. Set It's Position 1.5m Away From The Camera
        capsule.position = SCNVector3(0, 0, -1.5)
        
        //3. Add Our Cylinder To Our ARSCNView
        self.augmentedRealityView.scene.rootNode.addChildNode(capsule)
    }
    
    /// Adds A Custom Capsule To The Scene Hieracy With A Single Image For It's Material
    func addCapsuleWithImage(){
        
        //1. Create Our Capsule With A Single Image
        if let validImage = UIImage(named: "blackMirrorz"){
            
            let capsule = Capsule(content: validImage)
            
            //2. Set It's Position 1.5m Away From The Camera
            capsule.position = SCNVector3(0, 0, -1.5)
            
            //3. Add Our Cylinder To Our ARSCNView
            self.augmentedRealityView.scene.rootNode.addChildNode(capsule)
        }
    }
    
    
    /// Adds A Custom Torus To The Scene Hieracy With A Single Colour For It's Material
    func addTorus(){
        
        //1. Create A UITapGestureRecognizer & Add It To Our MainView
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap(rec:)))
        tapGesture.numberOfTapsRequired = 1
        self.view.addGestureRecognizer(tapGesture)
        
        //1. Create Our Rubber Floaty Thing With A Single Colour Which Tapers To The Top
        let singleColouredTorus = Torus(content: UIColor.red)
        
        //2. Set It's Position 1.5m Away From The Camera
        singleColouredTorus.position = SCNVector3(0, 0, -1.5)
        
        //3. Add Our Torus To Our ARSCNView
        self.augmentedRealityView.scene.rootNode.addChildNode(singleColouredTorus)
    }
    
        override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
            if(isPlacing) {
                    //1. Get The Current Touch Location In Our ARSCNView & Perform An ARSCNHitTest For Any Viable Feature Points
                    guard let currentTouchLocation = touches.first?.location(in: self.augmentedRealityView),
                          let hitTest = self.augmentedRealityView.hitTest(currentTouchLocation, types: .featurePoint).first else { return }
            
                    //2. Get The World Transform From The HitTest & Get The Positional Data From The Matrix (3rd Column)
                    let worldPositionFromTouch = hitTest.worldTransform.columns.3
            
//                    //3. Create An SCNNode At The Touch Location
            let nodeToPlace : SCNNode = ShapeBuilder.generateShape(shapeType: shapeTypeToPlace)
            nodeToPlace.position = SCNVector3(worldPositionFromTouch.x, worldPositionFromTouch.y, worldPositionFromTouch.z)
            
            self.augmentedRealityView.scene.rootNode.addChildNode(nodeToPlace)
            }
        }
    
    //Array To Store Any Added Nodes To The Scene Hierachy
    var nodesRendered = [SCNNode]()
    
    
    func performVirtualRaycast(){
        
        
    }
    /// Creates A Red, Blue & Green SCNNode
    func createNodes(){
        
        //1. Create A Red Sphere
        let redNode = SCNNode()
        let redGeometry = SCNSphere(radius: 0.2)
        redGeometry.firstMaterial?.diffuse.contents = UIColor.red
        redNode.geometry = redGeometry
        redNode.position = SCNVector3(-1.5, 0, -1.5)
        redNode.name = "RedNode"
        
        //2. Create A Green Sphere
        let greenNode = SCNNode()
        let greenGeometry = SCNSphere(radius: 0.2)
        greenGeometry.firstMaterial?.diffuse.contents = UIColor.green
        greenNode.geometry = greenGeometry
        greenNode.position = SCNVector3(0, 0, -1.5)
        greenNode.name = "GreenNode"
        
        //3. Create A Blue Sphere
        let blueNode = SCNNode()
        let blueGeometry = SCNSphere(radius: 0.2)
        blueGeometry.firstMaterial?.diffuse.contents = UIColor.blue
        blueNode.geometry = blueGeometry
        blueNode.position = SCNVector3(1.5, 0, -1.5)
        blueNode.name = "BlueNode"
        
        //4. Add Them To The Hierachy
        augmentedRealityView.scene.rootNode.addChildNode(redNode)
        augmentedRealityView.scene.rootNode.addChildNode(greenNode)
        augmentedRealityView.scene.rootNode.addChildNode(blueNode)
        
        //5. Store A Reference To The Nodes
        
        
        blueNode.highlightNodeWithDurarion(5)
        
        
    }
    
    
    /// Adds A Custom PlaneNode To The Scene Hierachy
    func addPlane(){
        
        //1. Create Our Plane Node
        let plane = Plane(content: UIColor.red,
                          doubleSided: true,
                          horizontal: false,
                          position: SCNVector3Zero)
        
        plane.position = SCNVector3(0, 0, -1.5)
        
        //3. Add Our Plane To Our ARSCNView
        self.augmentedRealityView.scene.rootNode.addChildNode(plane)
        
    }
    
    func createPlane(){
        
        let holderNode = SCNNode()
        
        holderNode.geometry = SCNPlane(width: 1, height: 0.5)
        
        let material = SCNMaterial()
        
        let viewToAdd = UIView(frame: CGRect(x: 0, y: 0, width: 1000, height: 500))
        viewToAdd.backgroundColor = .white
        
        let image = UIImage(named: "defaultGrid")
        let imageHolder = UIImageView(image: image)
        viewToAdd.addSubview(imageHolder)
        
        let bluelabel = UILabel(frame: CGRect(x: imageHolder.frame.size.width, y: 0, width: 500, height: 200))
        bluelabel.text = String("Naima")
        bluelabel.font = UIFont.systemFont(ofSize: 100)
        bluelabel.textColor = .red
        viewToAdd.addSubview(bluelabel)
        
        material.diffuse.contents = viewToAdd
        holderNode.geometry?.firstMaterial = material
        
        holderNode.position = SCNVector3(0, 0, -1.5)
        
        self.augmentedRealityView.scene.rootNode.addChildNode(holderNode)
        
    }
    
    
    //    /// Detects If A Node Is In View Of The Camera
    //    func detectNodeInFrustrumOfCamera(){
    //
    //        guard let cameraPointOfView = self.augmentedRealityView.pointOfView else { return }
    //
    //        var angle = cameraPointOfView.transform
    //
    //        print(angle)
    ////
    ////        // Translate first on -z direction
    ////        let translation = SCNMatrix4MakeTranslation(0, 0, 100)
    ////        // Rotate (yaw) around y axis
    ////        let rotation = SCNMatrix4MakeRotation(-1 * angle, 0, 1, 0)
    ////
    ////        // Final transformation: TxR
    ////        let transform = SCNMatrix4Mult(translation, rotation)
    ////
    ////        print(transform)
    //
    //    }
    
    /// Detects If A Node Is In View Of The Camera
    func detectNodeInFrustrumOfCamera(){
        
        guard let cameraPointOfView = self.augmentedRealityView.pointOfView else { return }
        
        for node in nodesRendered{
            
            if augmentedRealityView.isNode(node, insideFrustumOf: cameraPointOfView){
                
                print("\(node.name!) Is In View Of Camera")
                
            }else{
                
                print("\(node.name!) Is Not In View Of Camera")
            }
        }
        
    }
    
    
    
    @IBAction func takeScreenshotAction() {
        outputImageView.image = augmentedRealityView.snapshot()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        augmentedRealitySession.pause()
        augmentedRealitySession.run(configuration, options: runOptions(.ResetAndRemove))
        configuration.planeDetection = .vertical
        
    }
    
    
    
    
    //-----------------------
    //MARK: Geometry Creation
    //-----------------------
    
    
    func create(_ geometry: Geometry){
        
        
        
        switch geometry {
        
        case .Plane: break;
        //nodeToAdd = Plane(colour: .red, image: nil, doubleSided: true, horizontal: false)
        case .Box:
            nodeToAdd = Box(colours: [.red, .green, .blue, .black, .yellow, .cyan], images: nil)
        case .Sphere:
            nodeToAdd = Box(colours: [.red, .green, .blue, .black, .yellow, .cyan], images: nil)
        case .Pyramid:
            nodeToAdd = Box(colours: [.red, .green, .blue, .black, .yellow, .cyan], images: nil)
        case .Cone: break;
        //nodeToAdd = Cone(extends: true, colours: [.red, .cyan], images: nil)
        case .Cylinder: break;
        //nodeToAdd = Cylinder(colours: [.red, .green, .blue], images: nil)
        case .Capsule:
            break;
        case .Tube:
            nodeToAdd = Tube(colours: [.red, .cyan, .black, .green], images: nil)
        case .Torus:
            break;
        case .Text:
            nodeToAdd = Text(text: "Hello AR", colour: .white)
        case .ViewNode:
            nodeToAdd = ViewNode(width: 300, height: 300)
        case .VideoNode:
            let videoPath = Bundle.main.path(forResource: "TwinklCreate", ofType: "mp4")
            nodeToAdd = VideoNode(videoPath: videoPath!, doubleSided: true, horizontal: false)
            
        }
        currentNode = nodeToAdd
        nodeToAdd.position = SCNVector3(0, 0, -1.5)
        originalPosition = nodeToAdd.position
        augmentedRealityView.scene.rootNode.addChildNode(nodeToAdd)
        
    }
    
    
    
    var swipeIndex = -1
    var geometryCount = Geometry.count - 1
    
    //    var geometryArray = [
    //
    //        //Torus(colour: nil, image: UIImage(named:"defaultGrid")),
    //        //Plane(colour: .red, image: nil, doubleSided: true, horizontal: false)
    //        //Box(colours: [.red, .green, .blue, .black, .yellow, .cyan], images: nil),
    //        //Cone(extends: true, colours: [.red, .cyan], images: nil),
    //        //Pyramid(colours: [.red, .green, .blue, .black, .yellow], images: nil)
    //
    //    ]
    //
    /*
     Plane Width = 0.6
     Box = 0.6
     Sphere = 0.2
     Pyramid = 1.4
     
     Plane = 0
     Box = Plane Width (0.6) + 0.2(Spacer)
     3rd = Box Width (0.2) + Box.x
     4th =
     
     (0,0,0)
     (0.6 + 0.2, 0, 0)
     (0.8 + 0.2, 0, 0)
     
     */
    
    func displayeNodesInLine(){
        
        let holderNode = SCNNode()
        holderNode.position = SCNVector3(0, 0, -1.5)
        
        //1. Create The Text Node At The Top
        let text = Text(text: "Hello AR", colour: .white)
        holderNode.addChildNode(text)
        
        
        
        //2. Create Each Other Node And Place In A Line Below The Text
        
        var previousWidth: Float = 0
        
        
        //        for index in 0 ..< geometryArray.count {
        //
        //            //a. Create The Node
        //            let nodeToAdd = geometryArray[index]
        //
        //            //b. Get Its Bounding Box
        //            let (minVector, maxVector) = nodeToAdd.boundingBox
        //
        //            //c. Get The Width Of The Node
        //            let widthOfNode = maxVector.x - minVector.x
        //            nodeToAdd.position = SCNVector3(Float(index) * 0.5, 0 ,0)
        //            previousWidth = widthOfNode
        //            holderNode.addChildNode(nodeToAdd)
        //
        //            print(previousWidth)
        //        }
        
        holderNode.scale = SCNVector3(0.5, 0.5, 0.5)
        augmentedRealityView.scene.rootNode.addChildNode(holderNode)
        
    }
    
    
    //
    func getWidthOfNode(_ node: SCNNode) ->Float{
        
        let (minVector, maxVector) = node.boundingBox
        let max = maxVector.x - minVector.x
        return max
        
    }
    
    
    /// Sets The Current Swipe Index For Display Our SCNNodes
    ///
    /// - Parameter gesture: UISwipeGestureRecognizer)
    @objc func swipeToSelectGeometry(_ gesture: UISwipeGestureRecognizer){
        
        //1. Detect Which Direction We Are Swiping
        switch gesture.direction {
        
        case .left:
            
            //a. If We Are At The 1st Mode Then Increase The Swipe Index
            if swipeIndex == -1{
                
                swipeIndex += 1
                
            }else if swipeIndex != 0{
                //b. Else Decrease It
                swipeIndex -= 1
            }
            
        case .right:
            
            //2. If Our Swipe Index Doesnt Equal The Number Of Nodes In Our Enum Then Increase The Index
            if swipeIndex != geometryCount {
                
                swipeIndex += 1
            }
            
        default:
            break
        }
        
        //3. Remove Any Nodes Which We Have Added To Our Scene
        self.augmentedRealityView.scene.rootNode.enumerateChildNodes { (existingNode, _) in
            
            existingNode.removeFromParentNode()
        }
        
        //4. Get The Raw Value Of Our Geometry Enum From The Swipe Index
        guard let geometry = Geometry.init(rawValue: swipeIndex) else { return }
        
        //5. Create The Corresponding Geometry
        create(geometry)
        
    }
    
    @IBAction func generateNodes(_ controller: UISegmentedControl){
        
        augmentedRealityView.scene.rootNode.enumerateChildNodes { (existingNode, _) in
            existingNode.removeFromParentNode()
        }
        
        switch controller.selectedSegmentIndex {
        case 0: create(.Plane)
        case 1: create(.Box)
        case 2: create(.Sphere)
        case 3: create(.Pyramid)
        case 4: create(.Cone)
        case 5: create(.Cylinder)
        case 6: create(.Capsule)
        case 7: create(.Tube)
        case 8: create(.Torus)
        case 9: create(.Text)
            
        default:
            create(.Text)
        }
    }
    
    //---------------
    //MARK: ARSession
    //---------------
    
    /// Sets Up The ARSession
    func setupARSession(){
        
        //1. Set The AR Session
        augmentedRealityView.session = augmentedRealitySession
        augmentedRealityView.delegate = self
        
        //2. Conifgure The Type Of Plane Detection
        configuration.planeDetection = [planeDetection(.Horizontal)]
        
        //3. Configure The Debug Options
        augmentedRealityView.debugOptions = debug(.FeaturePoints)
        
        //4. If In Debug Mode Show Statistics
        #if DEBUG
        augmentedRealityView.showsStatistics = true
        #endif
        
        //5. Set The Scene Lighting
        augmentedRealityView.applyLighting()
        
        configuration.worldAlignment = .gravityAndHeading
        
        //6. Run The Session
        augmentedRealitySession.run(configuration, options: runOptions(.ResetAndRemove))
        
        
    }
    
}


//
//  ChapterTwo.swift
//  Setup
//
//  Created by Josh Robbins on 24/02/2018.
//  Copyright © 2018 BlackMirror. All rights reserved.
//

import UIKit
import ARKit

//-----------------------
//MARK: ARSCNViewDelegate
//-----------------------

extension ViewController: ARSCNViewDelegate{
    
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
    
        //1. Check The We Have An ARPlane Anchor
        guard let planeAnchor = anchor as? ARPlaneAnchor else { return }
       
        //2. Create Our Plane Node
        let planeNode = PlaneNode(anchor: planeAnchor, node: node, image: true, identifier: planes.keys.count, opacity: 1)
        
        //3. Store A Reference To It
        planes[planeAnchor] = planeNode
        
        //4. Set The Current Plane
        currentPlane = planeNode
        
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        
        DispatchQueue.main.async {
            
            //1. If Our Anchor Is An ARPlane Anchor & We Have Stored It
            guard let planeAnchor = anchor as? ARPlaneAnchor,
                let plane = self.planes[planeAnchor] else { return }
            
            //2. Set The Current Plane
            self.currentPlane = plane
            
            //3. Update Its Size & Data
            plane.update(planeAnchor)
            
        }
    }
    
    func renderer(_ renderer: SCNSceneRenderer, updateAtTime time: TimeInterval) {
        
        DispatchQueue.main.async {
            
            //1. Update The Tracking Status
            self.trackingStatusLabel.text = self.augmentedRealitySession.sessionStatus()
            
        }
    }
    
}

class ViewController: UIViewController {
    
    //1. Create A Reference To Our ARSCNView In Our Storyboard Which Displays The Camera Feed
    @IBOutlet weak var augmentedRealityView: ARSCNView!
    
    //2. Create A Reference To Our ARSCNView In Our Storyboard Which Will Display The ARSession Tracking Status
    @IBOutlet weak var trackingStatusLabel: UILabel!
    
    //3. Create A TextView To Hold Details About Any Detected Planes
    @IBOutlet var planeInformationHolder: UIView!
    @IBOutlet var planeInformationLabel: UITextView!
    @IBOutlet var planeConstraints:  NSLayoutConstraint!
    
    //3. Create Our ARWorld Tracking Configuration
    let configuration = ARWorldTrackingConfiguration()
    
    //4. Create Our Session
    let augmentedRealitySession = ARSession()
    
    var screenCenter: CGPoint!
    var currentPlane: PlaneNode!
    var planes = [ARPlaneAnchor: PlaneNode]()
    
    var keyBoardShown = false
    
    //--------------------
    //MARK: View LifeCycle
    //--------------------
    
    override func viewDidLoad() {
        
        setupARSession()
        
        //        let cube = CubeNode(width: 0.6,
        //                            height: 0.6,
        //                            length: 0.6)
        //        cube.position = SCNVector3(0, 0, -3)
        //        augmentedRealityView.scene.rootNode.addChildNode(cube)
        //        cube.rotate(.YAxis)
        //        let _ = cube.calculatedSize()
        
        //let plane = Plane(colour: .red, doubleSided: true,  horizontal: true)
        //augmentedRealityView.scene.rootNode.addChildNode(plane)
        //plane.calculatedSizeOfPlane()
        
        let cube = CubeSizeNode()
        cube.position = SCNVector3(0, 0, -3)
        augmentedRealityView.scene.rootNode.addChildNode(cube)
        
        
        super.viewDidLoad()
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        augmentedRealitySession.pause()
        augmentedRealitySession.run(configuration, options: runOptions(.ResetAndRemove))
        planes.removeAll()
        hidePlaneInformationLabel(true)
    }
    
    //---------------
    //MARK: ARSession
    //---------------
    
    /// Sets Up The ARSession
    func setupARSession(){
        
        //1. Set The AR Session
        augmentedRealityView.session = augmentedRealitySession
        augmentedRealityView.delegate = self
        
        //2. Conifgure The Type Of Plane Detection
        configuration.planeDetection = [planeDetection(.Both)]
        
        //3. Configure The Debug Options
        augmentedRealityView.debugOptions = debug(.FeaturePoints)
        
        //4. If In Debug Mode Show Statistics
        #if DEBUG
        augmentedRealityView.showsStatistics = true
        planeConstraints.constant = 22
        #endif
        
        //5. Hide The Plane Information Until A Plane Is Shown
        hidePlaneInformationLabel(true)
        
        //6. Run The Session
        augmentedRealitySession.run(configuration, options: runOptions(.ResetAndRemove))
        
    }
    
    
    //-----------------------
    //MARK: Touch Interaction
    //-----------------------
    
    func vector3FromARHitTestResult(_ matrix: ARHitTestResult) -> SCNVector3{
        
        let x = matrix.worldTransform.columns.3.x
        let y = matrix.worldTransform.columns.3.y
        let z = matrix.worldTransform.columns.3.z
        
        return SCNVector3(x, y, z)
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
   
        
//        guard let touchLocation = touches.first?.location(in: augmentedRealityView) else { return }
//
//        if !keyBoardShown{
//
//            let hitTestResults = augmentedRealityView.hitTest(touchLocation, types: .existingPlaneUsingExtent)
//
//            guard let hitTestResult = hitTestResults.first else { return }
//
//            let position = vector3FromARHitTestResult(hitTestResult)
//
//            guard let keyBoardModel = SCNScene(named: "ARModels.scnassets/Keyboard.scn"),
//                let keyBoard = keyBoardModel.rootNode.childNode(withName: "Root", recursively: false) else{ return }
//
//            augmentedRealityView.scene.rootNode.addChildNode(keyBoard)
//            keyBoard.position = position
//
//            keyBoardShown = true
//
//            return
//
//        }else{
//
//            guard let hitTest = augmentedRealityView.hitTest(touchLocation, options: nil).first,
//                let nodeName = hitTest.node.name?.replacingOccurrences(of: "Button", with: "")
//                else { return }
//
//            print(nodeName)
//
//        }
        
    }
    
    /*
     override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
     
     /*
     1. Get The Current Touch Location
     2. Check That We Have Touched A Valid Node
     3. Check That Our Touched Object Is An ARPlane Anchor
     4. Determine If It Is One That Has Been Stored
     */
     
     guard let touchLocation = touches.first?.location(in: augmentedRealityView),
     let hitTest = augmentedRealityView.hitTest(touchLocation, types: .existingPlaneUsingExtent).first,
     let planeAnchor = hitTest.anchor as? ARPlaneAnchor,
     let detectedPlane = self.planes[planeAnchor]
     else {
     // No Valid Plane Has Been Detected So Hide The Plane Information Label
     self.hidePlaneInformationLabel(true)
     return
     
     }
     
     //We Have A Valid Plane So Display It's Current Info
     currentPlane = detectedPlane
     hidePlaneInformationLabel(false)
     displayPlaneInfo()
     
     }
     */
    
    //-----------------------
    //MARK: Plane Information
    //-----------------------
    
    /// Displays The Current Width, Height, & Alignment Info Of The Plane
    func displayPlaneInfo(){
        
        //1. Uodate The Plane Information Label
        DispatchQueue.main.async {
            
            guard let width = self.currentPlane.widthInfo,
                let height = self.currentPlane.heightInfo,
                let alignmentInfo = self.currentPlane.alignmentInfo else { return }
            
            self.planeInformationLabel.text = """
            Width Of Plane =  \(width)
            Height Of Plane =  \(height)
            Plane Alignment = \(alignmentInfo)
            """
        }
        
        //2. Hide It After 5 Seconds
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            self.hidePlaneInformationLabel(true)
        }
        
    }
    
    /// Shows Or Hides The Plane Information Area
    ///
    /// - Parameter enabled: Bool
    func hidePlaneInformationLabel(_ enabled: Bool){
        
        planeInformationLabel.isHidden = enabled
        planeInformationHolder.isHidden = enabled
        
    }
    
}



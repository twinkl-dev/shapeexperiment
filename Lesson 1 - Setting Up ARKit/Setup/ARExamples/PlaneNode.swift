//  Plane.swift
//  Setup
//
//  Created by Josh Robbins on 17/02/2018.
//  Copyright © 2018 BlackMirror. All rights reserved.
//

import UIKit
import ARKit

class PlaneNode: SCNNode {

    let DEFAULT_IMAGE: String = "defaultGrid"
    let NAME: String = "PlaneNode"
    var planeGeometry: SCNPlane
    var planeAnchor: ARPlaneAnchor
    
    var widthInfo: String!
    var heightInfo: String!
    var alignmentInfo: String!
    
    //---------------
    //MARK: LifeCycle
    //---------------
    
    /// Inititialization
    ///
    /// - Parameters:
    ///   - anchor: ARPlaneAnchor
    ///   - node: SCNNode
    ///   - node: Bool
    init(anchor: ARPlaneAnchor, node: SCNNode, image: Bool, identifier: Int, opacity: CGFloat = 0.25){
        
        //1. Create The SCNPlaneGeometry
        self.planeAnchor = anchor
        self.planeGeometry = SCNPlane(width: CGFloat(anchor.extent.x), height: CGFloat(anchor.extent.z))
        let planeNode = SCNNode(geometry: planeGeometry)
        
        super.init()
    
        //2. If The Image Bool Is True We Use The Default Image From The Assets Bundle
        let planeMaterial = SCNMaterial()
        
        if image{
            
            planeMaterial.diffuse.contents = UIImage(named: DEFAULT_IMAGE)
            
        }else{
            
            planeMaterial.diffuse.contents = UIColor.cyan
        }
        
        //3. Set The Geometries Contents
        self.planeGeometry.materials = [planeMaterial]
        
        //4. Set The Position Of The PlaneNode
        planeNode.simdPosition = float3(self.planeAnchor.center.x, 0, self.planeAnchor.center.z)
        
        //5. Rotate It On It's XAxis
        planeNode.eulerAngles.x = -.pi / 2
        
        //6. Set The Opacity Of The Node
        planeNode.opacity = opacity
       
        //7. Add The PlaneNode
        node.addChildNode(planeNode)
        
        //8. Set The Nodes ID
        node.name = "\(NAME) \(identifier)"
        
    }
    
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    
    /// Updates The Size Of The Plane As & When The ARPlaneAnchor Has Been Updated
    ///
    /// - Parameter anchor: ARPlaneAnchor
    func update(_ anchor: ARPlaneAnchor) {
      
        self.planeAnchor = anchor
        
        self.planeGeometry.width = CGFloat(anchor.extent.x)
        self.planeGeometry.height = CGFloat(anchor.extent.z)
        
        self.position = SCNVector3Make(anchor.center.x, 0.01, anchor.center.z)
        
        returnPlaneInfo()
    }
    
    //-----------------------
    //MARK: Plane Information
    //-----------------------

    /// Returns The Size Of The ARPlaneAnchor & Its Alignment
    func returnPlaneInfo(){
        
        let widthOfPlane = self.planeAnchor.extent.x
        let heightOfPlane = self.planeAnchor.extent.z
        
        var planeAlignment: String!
        
        switch planeAnchor.alignment {
            
        case .horizontal:
            planeAlignment = "Horizontal"
        case .vertical:
            planeAlignment = "Vertical"
        }
    
        #if DEBUG
        print("""
            Width Of Plane =  \(String(format: "%.2fm", widthOfPlane))
            Height Of Plane =  \(String(format: "%.2fm", heightOfPlane))
            Plane Alignment = \(planeAlignment)
            """)
        #endif
 
        self.widthInfo = String(format: "%.2fm", widthOfPlane)
        self.heightInfo = String(format: "%.2fm", heightOfPlane)
        self.alignmentInfo = planeAlignment
    }
    
}

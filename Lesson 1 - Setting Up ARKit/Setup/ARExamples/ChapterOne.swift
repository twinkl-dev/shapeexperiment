//
//  ChapterOne.swift
//  Setup
//
//  Created by Josh Robbins on 17/02/2018.
//  Copyright © 2018 BlackMirror. All rights reserved.
//

import UIKit
import ARKit

//-----------------------
//MARK: ARSCNViewDelegate
//-----------------------

extension ViewController: ARSCNViewDelegate{
    
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) { }
    
    func renderer(_ renderer: SCNSceneRenderer, updateAtTime time: TimeInterval) {
        
        DispatchQueue.main.async {
            
            //1. Update The Tracking Status
            self.trackingStatusLabel.text = self.augmentedRealitySession.sessionStatus()
          
        }
    }

}

class ViewController: UIViewController {

    //1. Create A Reference To Our ARSCNView In Our Storyboard Which Displays The Camera Feed
    @IBOutlet weak var augmentedRealityView: ARSCNView!
    
    //2. Create A Reference To Our ARSCNView In Our Storyboard Which Will Display The ARSession Tracking Status
    @IBOutlet weak var trackingStatusLabel: UILabel!
    
    //3. Create Our ARWorld Tracking Configuration
    let configuration = ARWorldTrackingConfiguration()
    
    //4. Create Our Session
    let augmentedRealitySession = ARSession()
    
    //--------------------
    //MARK: View LifeCycle
    //--------------------
  

    
    override func viewDidLoad() {
        
        
        super.viewDidLoad()
    
        //addMultiColouredCylinder()
        
    }
 
    

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
     
    }

    override func viewDidAppear(_ animated: Bool) {
        
          setupARSession()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        augmentedRealitySession.pause()
        augmentedRealitySession.run(configuration, options: runOptions(.ResetAndRemove))

    }
   
    //---------------
    //MARK: ARSession
    //---------------
    
    /// Sets Up The ARSession
    func setupARSession(){
    
        //1. Set The AR Session
        augmentedRealityView.session = augmentedRealitySession
        augmentedRealityView.delegate = self
        
        //2. Conifgure The Type Of Plane Detection
        configuration.planeDetection = [planeDetection(.Both)]
        
        //3. Configure The Debug Options
        augmentedRealityView.debugOptions = debug(.FeaturePoints)
       
        //4. If In Debug Mode Show Statistics
        #if DEBUG
        augmentedRealityView.showsStatistics = true
        #endif
        
        //5. Run The Session
        augmentedRealitySession.run(configuration, options: runOptions(.ResetAndRemove))
        
    }
    
}


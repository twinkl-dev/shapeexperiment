//
//  SCNNode+Extensions.swift
//  Setup
//
//  Created by Josh Robbins on 18/02/2018.
//  Copyright © 2018 BlackMirror. All rights reserved.
//

import Foundation
import ARKit
import UIKit

extension SCNNode{
    
    /// Determines The Tyoe Of Rotation Needed
    enum Rotation {
        
        case XAxis, YAxis

        
        /// Returns The SCNVector For Our Desired Rotation
        var vector: SCNVector3{
            
            switch self {
            case .XAxis:
                return SCNVector3(1, 0, 0)
            case .YAxis:
                return SCNVector3(0, 1, 0)
            }
        }
    }
  
    //----------------
    //MARK: Animations
    //----------------
    
    /// Rotates The SCNNode By 360 Degrees
    /// Along Either The X Or Y Axis
    ///
    /// - Parameter rotation: Rotation
    func rotate(_ rotation: Rotation){
        
        //1. Create An SCNAction Which Will Rotate The Object 360 Degress Around The Desired Axis Over A Course Of 10 Seconds
        let rotateAction = SCNAction.rotate(by: .pi * 2, around: rotation.vector, duration: 10)
        
        //2. Run The Action
        self.runAction(rotateAction)
        
    }
 
    
    /// Rotates The SCNNode By A Specified Angle Of Degrees
    /// Along Either The X Or Y Axis
    /// - Parameters:
    ///   - degrees: CGFloat
    ///   - rotation: Rotation
    ///   - animated: Bool
    func rotateBy(_ degrees: CGFloat, rotation: Rotation, animated: Bool){
        
        // Convert Degrees To Radians (1 Degree Is Equivalent To pi/180 radians)
        let rotationAsRadian = degrees * .pi/180
        
        if animated{
    
            //1a. Create An SCNAction Which Will Rotate The Object 360 Degress Around The Desired Axis Over A Course Of 10 Seconds
            let rotateAction = SCNAction.rotate(by: rotationAsRadian, around: rotation.vector, duration: 5)
            
            //1b. Run The Action
            self.runAction(rotateAction)
            
        }else{
            
            //2a. Get The X, Y, Z Values Of The Desired Rotation
            let vector3x = rotation.vector.x
            let vector3y = rotation.vector.y
            let vector3z = rotation.vector.z
            
            //2b. Set The Position & Rotation Of The Object
            self.rotation = SCNVector4Make(vector3x, vector3y, vector3z, Float(rotationAsRadian))
        }
       
    }
    
    /// Doubles The Size Of The SCNNode & Then Returns It To Its Original Size
    func growAndShrink(){
        
        //1. Create An SCNAction Which Will Double The Size Of Our Node
        let growAction = SCNAction.scale(by: 2, duration: 5)
        
        //2. Create Another SCNAction Wjich Will Revert Our Node Back To It's Original Size
        let shrinkAction = SCNAction.scale(by: 0.5, duration: 5)
        
        //3. Create An Animation Sequence Which Will Store Our Actions
        let animationSequence = SCNAction.sequence([growAction, shrinkAction])
        
        //4. Run The Sequence
        self.runAction(animationSequence)
        
    }
    
    func higlightNode(){
        
        let currentMaterial = self.geometry?.firstMaterial
        
        SCNTransaction .begin()
        
        SCNTransaction.animationDuration = 2
        
        SCNTransaction.completionBlock = {
            SCNTransaction .begin()
            SCNTransaction.animationDuration = 2
            
            currentMaterial?.emission.contents = UIColor.black
            
            SCNTransaction.commit()
        }
         currentMaterial?.emission.contents = UIColor.red
        
        SCNTransaction.commit()
        
    }
    
    func bob(){
        
        let moveUpAction = SCNAction.move(to: SCNVector3(0,1,0), duration: 2)
        let moveDownAction = SCNAction.move(to: SCNVector3Zero, duration: 2)
        let bobSequence = SCNAction.sequence([moveUpAction, moveDownAction])
        let infiniteLoop = SCNAction.repeatForever(bobSequence)
        self.runAction(infiniteLoop)
    
    }
        
    //-----------------------
    //MARK: Size Calculations
    //-----------------------
    
    /// Calculates The Size & Volume Of The Cube
    ///
    /// - Returns: Tuole - (length: Float, width: Float, height: Float, volume: Float)
    func calculatedSizeOfCube() -> (length: Float, width: Float, height: Float, volume: Float){
        
        let (minVector, maxVector) = self.boundingBox
        let length = maxVector.z - minVector.z
        let width = maxVector.x - minVector.x
        let height = maxVector.y - minVector.y
        let volume = length * width * height
        
        print("""
            Length Of Cube = \(length)
            Width Of Cube = \(width)
            Height Of Cube = \(height)
            Volume Of Cube = \(volume)
            """)
        
        return (length: length, width: width, height: height, volume: volume)
    }
    
    /// Calculates The Size & Area Of The Plane
    ///
    /// - Returns: Tuple (height: Float, width: Float, area: Float)
    func calculatedSizeOfPlane() -> (height: Float, width: Float, area: Float){
        
        let (minVector, maxVector) = self.boundingBox
        let height = maxVector.y - minVector.y
        let width = maxVector.x - minVector.x
        let area = width * height
        
        print("""
            Height Of Plane = \(height)
            Width Of Plane = \(width)
            Area Of Plane = \(area)
            """)
        
        return (height: height, width: width, area: area)
    }
}

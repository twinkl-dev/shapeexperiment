//
//  ShapeBuilder.swift
//  Setup
//
//  Created by Tarantino O'Connor on 13/10/2020.
//  Copyright © 2020 BlackMirror. All rights reserved.
//

import Foundation
import UIKit
import ARKit
import CoreMotion
import WebKit

enum ShapeType: String {
    case cone, cuboid, cylinder, squarePyramid, triangularPrism, sphere
    
    func fromString(_ value: String) -> ShapeType {
        
        return { () -> ShapeType in
            switch value {
            case "cone":
                return .cone
            case "cuboid":
                return .cuboid
            case "cylinder":
                return .cylinder
            case "triangularPrism":
                return .triangularPrism
            case "squarePyramid":
                return .squarePyramid
            case "sphere":
                return .sphere
            default:
                return .cone
            }
        }()
    }
}

struct ShapeData {
    var vertices : String
    var apex: String
    var symmetry: String
    var realLifeExamples: String
    var needsShader : Bool
}

protocol ShapeDataRepresentable {
    var shapeData: ShapeData { get set }
    var shapeName: String { get set }
}

class ShapeBuilder
{
    static func generateShape(shapeType: ShapeType) -> SCNNode {
        switch shapeType {
        case .cone:
            return generateCone()
        case .cuboid:
            return generateCuboid()
        case .cylinder:
            return generateCylinder()
        case .squarePyramid:
            return generateSquarePyramid()
        case .sphere:
            return generateSphere()
        case .triangularPrism:
            return generateTriangularPrism()
        }
    }
    
    static func applyShader(_ geometry:SCNGeometry){
        
        
        guard let path = Bundle.main.path(forResource: "wireframe_shader", ofType: "metal",  inDirectory: "art.scnassets"), let shader = try? String(contentsOfFile: path, encoding: .utf8) else {
            print("cannot find file")
            return }
        
        geometry.materials.forEach { (material) in
            material.shaderModifiers = [.surface: shader]
        }
        
        // For one material geometries.
        //geometry.firstMaterial?.shaderModifiers = [.surface: shader]
    }
    
    static func generateCone() -> SCNNode {
        return ConeNode()
    }
    
    static func generateCuboid() -> SCNNode {
        return CuboidNode()
    }
    
    static func generateCylinder() -> SCNNode {
        return CylinderNode()
    }
    
    static func generateTriangularPrism() -> SCNNode {
        
        let triangularPrismNode = TriangularPrismNode()
        
        triangularPrismNode.scale = SCNVector3(0.125,0.125,0.125)
        
        return triangularPrismNode
    }
    
    static func generateSquarePyramid() -> SCNNode {
        return SquarePyramidNode()
    }
    
    static func generateSphere() -> SCNNode {
        return SphereNode()
    }
    
}

class ConeNode: SCNNode, ShapeDataRepresentable {
    var shapeData: ShapeData = ShapeData(vertices: "0", apex: "1", symmetry: "Infinite", realLifeExamples: "ice-cream cones, party hats", needsShader: true)
    var shapeName: String = "Cone"
    {
        didSet{name = shapeName}
    }
    private var faceArray = [SCNMaterial]()
    
    /// Creates An SCNCone With Colours For It's Base & Body
    /// (An Array [UIColour] Must Be Input)
    /// - Parameters:
    ///   - extends: Bool (If extends == false The Cone Will Taper To The Top Else It Will Extend To The Top)
    ///   - topRadius: Optional CGFloat (Defaults To 0cm)
    ///   - bottomRadius: Optional CGFloat (Defaults To 30cm)
    ///   - contents: Optional [Any] - [Body, Top, Bottom]
    init(extends: Bool = false, topRadius: CGFloat = 0, bottomRadius: CGFloat = 0.15, height: CGFloat = 0.25, contents:[Any] = [UIColor.purple, UIColor.green]) {
        
        super.init()
        
        //1. Declare A Variable So We Can Extend Or Taper The Cone
        var adjustedTopRadius = topRadius
        var adjustedBottomRadius = bottomRadius
        
        //2. If The Cone Needs To Extend To The Top Invert The Initial Values
        if extends{
            
            adjustedTopRadius = bottomRadius
            adjustedBottomRadius = topRadius
        }
        
        //3. Create The Cone Geometry With Our TopRadius, BottomRadius & Height Paramaters
        self.geometry = SCNCone(topRadius: adjustedTopRadius, bottomRadius: adjustedBottomRadius, height: height)
        
        //4. Create A Temporary Array To Store Either Our UIColors
        var sideArray = [Any]()
        
        //5. Assign The Colours
        if let colours = contents as? [UIColor], colours.count == 2{
            
            //The Materials Will Be A UIColor
            sideArray = colours
            
        }else{
            //Set Our Material Colours To Cyan, Red
            sideArray = [UIColor.cyan, UIColor.red]
        }
        
        //6. Loop Through The Number Of Faces & Create A New Material For Each
        for index in 0 ..< 2{
            let face = SCNMaterial()
            face.isDoubleSided = true
            face.diffuse.contents = sideArray[index]
            face.lightingModel = SCNMaterial.LightingModel.constant
            //Add The Material To Our Face Array
            faceArray.append(face)
        }
        
        //7. Set The Cones Materials From Our Face Array
        self.geometry?.materials = faceArray
        
        if(shapeData.needsShader)
        {
            ShapeBuilder.applyShader(self.geometry!)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class CuboidNode : SCNNode, ShapeDataRepresentable {
    var shapeData: ShapeData = ShapeData(vertices: "8", apex: "0", symmetry: "9", realLifeExamples: "dice, sugar cubes, rubik's cubes", needsShader: true)
    var shapeName: String = "Cuboid"
    {
        didSet{name = shapeName}
    }
    private var faceArray = [SCNMaterial]()
    
    /// Creates An SCNBox With Either A Colour Or UIImage For Each Of It's Faces
    /// (Either An Array [Colour] Or [UIImage] Must Be Input)
    /// - Parameters:
    ///   - width: Optional CGFloat (Defaults To 20cm)
    ///   - height: Optional CGFloat (Defaults To 20cm)
    ///   - length: Optional CGFloat (Defaults To 20cm)
    ///   - chamferRadius: Optional CGFloat (Defaults To 0cm)
    ///   - colours: Optional [UIColor] - [Front, Right, Back, Left, Top, Bottom]
    init(width: CGFloat = 0.2, height: CGFloat = 0.2, length: CGFloat = 0.2, chamferRadius: CGFloat = 0, colours: [UIColor]? = [UIColor.red, UIColor.green, UIColor.blue, UIColor.purple, UIColor.yellow, UIColor.systemPink]) {
        
        super.init()
        
        //1. Create The Box Geometry With Our Width, Height, Length & Chamfer Radius Parameters
        self.geometry = SCNBox(width: width, height: height, length: length, chamferRadius: chamferRadius)
        
        //2. Create A Temporary Array To Store Either Our UIColors
        var sideArray = [Any]()
        
        
        //Our Side Array Will Be Equal To Our Colours Array
        guard let coloursArray = colours else { return }
        sideArray = coloursArray
        
        
        //4. Loop Through The Number Of Faces & Create A New Material For Each
        for index in 0 ..< 6{
            let face = SCNMaterial()
            face.isDoubleSided = true
            face.diffuse.contents = sideArray[index]
            face.lightingModel = SCNMaterial.LightingModel.constant
            //Add The Material To Our Face Array
            faceArray.append(face)
        }
        
        //5. Set The Boxes Materials From Our Face Array
        self.geometry?.materials = faceArray
        
        if(shapeData.needsShader)
        {
            ShapeBuilder.applyShader(self.geometry!)
        }
        
        name = shapeName
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

class SphereNode: SCNNode{
    
    var shapeData: ShapeData = ShapeData(vertices: "0", apex: "0", symmetry: "Infinite", realLifeExamples: "ball, bubble", needsShader: false)
    var shapeName: String = "Sphere"
    {
        didSet{name = shapeName}
    }
    /// Creates An SCNSphere With Either A Single Colour Or Image For It's Material
    /// (Either A Colour Or UIImage Must Be Input)
    /// - Parameters:
    ///  - radius: Optional CGFloat (Defaults To 30cm)
    /// - content: Any (UIColor Or UIImage)
    init(radius: CGFloat = 0.075, content: Any = UIColor.orange) {
        
        super.init()
        
        //1. Create The Sphere Geometry With Our Radius Parameter
        self.geometry = SCNSphere(radius: radius)
        
        //2. Create A New Material
        let material = SCNMaterial()
        
        material.isDoubleSided = true
       // material.lightingModel = SCNMaterial.LightingModel.constant
        
        //3. If A Colour Has Not Be Set The Then Material Will Be A UIImage
        if let colour = content as? UIColor{
            
            //The Material Will Be A UIColor
            material.diffuse.contents = colour
            
        } else {
            //Set Our Material Colour To Cyan
            material.diffuse.contents = UIColor.cyan
        }
        
        //4. Set The Material Of The Sphere
        self.geometry?.firstMaterial = material
        if(shapeData.needsShader)
        {
            ShapeBuilder.applyShader(self.geometry!)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

class TriangularPrismNode : SCNNode, ShapeDataRepresentable {
    var shapeData: ShapeData = ShapeData(vertices: "4", apex: "1", symmetry: "4", realLifeExamples: "tents, gift packaging", needsShader: false)
    var shapeName: String = "Triangular Prism"
    {
        didSet{name = shapeName}
    }
    private var faceArray = [SCNMaterial]()
    /// Creates An SCNPyramid With A Different Material On Each Of Its 5 Faces
    /// (Either An Array [Colour] Or [UIImage] Must Be Input)
    /// - Parameters:
    ///   - width: Optional CGFloat (Defaults To 30cm)
    ///   - height: Optional CGFloat (Defaults To 60cm)
    ///   - length: Optional CGFloat (Defaults To 30cm)
    ///   - contents: [Any] - [Front, Right , Left, Back, Bottom]
    init(width: CGFloat = 0.3, height: CGFloat = 0.6, length: CGFloat = 0.3, contents: [Any] = [UIColor.red, UIColor.orange, UIColor.green, UIColor.blue, UIColor.purple]) {
        
        super.init()
        
        // Create vertices.
        let t = 0.5
        
        let vertices : [SCNVector3] = [
            SCNVector3(t,0.5,-0.5),
            SCNVector3(0,0.5,0.5),
            SCNVector3(-t,0.5,-0.5),
            
            SCNVector3(t,-0.5,-0.5),
            SCNVector3(0,-0.5,0.5),
            SCNVector3(-t,-0.5,-0.5)
        ]
        
        let source = SCNGeometrySource(vertices: vertices)
        
        
        // Create indicies.
        let indices: [UInt32] = [
            // 1st Tri
            0,1,2,
            // Bottom
            2,4,5,
            5,1,2,
            2,4,1,
            // Else
            0,3,5,
            0,3,4,
            4,0,1,
            2,0,5,
            // 2nd Tri
            3,5,4
        ]
        
        let element = SCNGeometryElement(indices: indices, primitiveType: .triangles)
        
        self.geometry = SCNGeometry(sources: [source], elements: [element])
        
        //2. Create A Temporary Array To Store Either Our UIColors Or UIImages
        var sideArray = [Any]()
        
        //3. Assign The Colours Or UIImages
        if let colours = contents as? [UIColor], colours.count == 5{
            
            //The Materials Will Be A UIColor
            sideArray = colours
            
        }else{
            
            //Set Our Material Colours To Red, Orange, Green, Blue, Purple
            sideArray = [UIColor.red, UIColor.orange, UIColor.green, UIColor.blue, UIColor.purple]
            
        }
        //5. Loop Through The Number Of Faces & Create A New Material For Each
        for index in 0 ..< sideArray.count{
            let face = SCNMaterial()
            face.isDoubleSided = true
            face.diffuse.contents = sideArray[index]
            //face.lightingModel = SCNMaterial.LightingModel.constant
            //Add The Material To Our Face Array
            faceArray.append(face)
        }
        
        self.geometry?.materials = faceArray
        
        if(shapeData.needsShader)
        {
            ShapeBuilder.applyShader(self.geometry!)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class SquarePyramidNode : SCNNode, ShapeDataRepresentable {
    var shapeData: ShapeData = ShapeData(vertices: "5", apex: "1", symmetry: "4", realLifeExamples: "pyramids of Giza in Egypt", needsShader: true)
    var shapeName: String = "Square Pyramid"
    {
        didSet{name = shapeName}
    }
    private var faceArray = [SCNMaterial]()
    /// Creates An SCNPyramid With A Different Material On Each Of Its 5 Faces
    /// (Either An Array [Colour] Or [UIImage] Must Be Input)
    /// - Parameters:
    ///   - width: Optional CGFloat (Defaults To 30cm)
    ///   - height: Optional CGFloat (Defaults To 60cm)
    ///   - length: Optional CGFloat (Defaults To 30cm)
    ///   - contents: [Any] - [Front, Right , Left, Back, Bottom]
    init(width: CGFloat = 0.3, height: CGFloat = 0.6, length: CGFloat = 0.3, contents: [Any] = [UIColor.red, UIColor.orange, UIColor.green, UIColor.blue, UIColor.purple]) {
        
        super.init()
        
        //1. Create The Pyramid Geometry With Our Width, Height & Length Parameters
        self.geometry = SCNPyramid(width: width, height: height, length: length)
        
        //2. Create A Temporary Array To Store Either Our UIColors Or UIImages
        var sideArray = [Any]()
        
        //3. Assign The Colours Or UIImages
        if let colours = contents as? [UIColor], colours.count == 5{
            
            //The Materials Will Be A UIColor
            sideArray = colours
            
        }else{
            
            //Set Our Material Colours To Red, Orange, Green, Blue, Purple
            sideArray = [UIColor.red, UIColor.orange, UIColor.green, UIColor.blue, UIColor.purple]
            
        }
        //5. Loop Through The Number Of Faces & Create A New Material For Each
        for index in 0 ..< 5{
            let face = SCNMaterial()
            face.isDoubleSided = true
            face.diffuse.contents = sideArray[index]
            face.lightingModel = SCNMaterial.LightingModel.constant
            //Add The Material To Our Face Array
            faceArray.append(face)
        }
        
        //6. Set The Pyramids Materials From Our Face Array
        self.geometry?.materials = faceArray
        
        if(shapeData.needsShader)
        {
            ShapeBuilder.applyShader(self.geometry!)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class CylinderNode : SCNNode, ShapeDataRepresentable {
    var shapeData: ShapeData = ShapeData(vertices: "0", apex: "0", symmetry: "Infinite", realLifeExamples: "cans, tins", needsShader: true)
    var shapeName: String = "Cylinder"
    {
        didSet {
            name = shapeName
        }
    }
    private var faceArray = [SCNMaterial]()
    
    /// Creates An SCNCylinder With Either A Colour Or UIImage For It's Base, Top & Body
    /// (Either An Array [Colour] Or [UIImage] Must Be Input)
    /// - Parameters:
    ///   - radius: Optional CGFloat (Defaults To 30cm)
    ///   - height: Optional CGFloat (Defaults To 60cm)
    ///   - contents: Optional [Any] - [Body, Top, Bottom]
    init(radius: CGFloat = 0.3, height: CGFloat = 0.6, contents: [Any] = [UIColor.red]) {
        
        super.init()
        
        //1. Create The Cylinder Geometry With Our Radius & Height Parameters
        self.geometry = SCNCylinder(radius: radius, height: height)
        
        //2. Create A Temporary Array To Store Either Our UIColors Or UIImages
        var sideArray = [Any]()
        
        //3. Assign The Colours Or UIImages
        if let colours = contents as? [UIColor], colours.count == 3{
            
            //The Materials Will Be A UIColor
            sideArray = colours
            
        }else{
            
            //Set Our Material Colours To Cyan, Red, Green
            sideArray = [UIColor.cyan, UIColor.red, UIColor.green]
            
        }
        
        //4. Loop Through The Number Of Faces & Create A New Material For Each
        for index in 0 ..< 3{
            let face = SCNMaterial()
            face.isDoubleSided = true
            face.lightingModel = SCNMaterial.LightingModel.constant
            
            face.diffuse.contents = sideArray[index]
            
            //Add The Material To Our Face Array
            faceArray.append(face)
        }
        
        //6. Set The Cylinders Materials From Our Face Array
        self.geometry?.materials = faceArray
        if(shapeData.needsShader)
        {
            ShapeBuilder.applyShader(self.geometry!)
        }
    }
    
    required init?(coder aDecoder: NSCoder) { fatalError("Cylinder Node Coder Not Implemented") }
}



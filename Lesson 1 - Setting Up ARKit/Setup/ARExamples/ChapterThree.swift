//
//  ChapterThree.swift
//  Setup
//
//  Created by Josh Robbins on 25/02/2018.
//  Copyright © 2018 BlackMirror. All rights reserved.
//

import UIKit
import ARKit

//-----------------------
//MARK: ARSCNViewDelegate
//-----------------------

extension ChapterThree: ARSCNViewDelegate{
    
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
    
        
    }
    
    func renderer(_ renderer: SCNSceneRenderer, updateAtTime time: TimeInterval) {
        
        DispatchQueue.main.async {
            
            //1. Update The Tracking Status
            self.trackingStatusLabel.text = self.augmentedRealitySession.sessionStatus()
            
        }
    }
    
}

class ChapterThree: UIViewController {    
    //1. Create A Reference To Our ARSCNView In Our Storyboard Which Displays The Camera Feed
    @IBOutlet weak var augmentedRealityView: ARSCNView!
    
    //2. Create A Reference To Our ARSCNView In Our Storyboard Which Will Display The ARSession Tracking Status
    @IBOutlet weak var trackingStatusLabel: UILabel!
    
    //3. Create Our ARWorld Tracking Configuration
    let configuration = ARWorldTrackingConfiguration()
    
    //4. Create Our Session
    let augmentedRealitySession = ARSession()
    

    //--------------------
    //MARK: View LifeCycle
    //--------------------
    
    override func viewDidLoad() {
        
        setupARSession()
        
        loadModel(.Gengar)
        
        super.viewDidLoad()
     
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        
    }
    
    
    /// Pokemon Model Scene Name
    ///
    /// - Pikachu
    /// - ScatterBug
    /// - Gengar
    enum PokemonName: String{
        
        case Pikachu
        case ScatterBug
        case Gengar
        case Flatty
    }
    
    
    /// Loads An SCNScene And Adds It The ARSCNView
    ///
    /// - Parameter pokemon: PokemonName
    func loadModel(_ pokemon: PokemonName){
        
        let modelPath = "ARModels.scnassets/\(pokemon.rawValue).scn"
        
        //1. Get The Reference To Our SCNScene & Get The Model Root Node
        guard let model = SCNScene(named: modelPath),
              let pokemonModel = model.rootNode.childNode(withName: "Pokemon", recursively: false) else { return }
        
        //2.Add It To Our SCNView
        augmentedRealityView.scene.rootNode.addChildNode(pokemonModel)
        
        //3. Scale The Model
        switch pokemon {

        case .Pikachu, .ScatterBug:
             pokemonModel.scale = SCNVector3(0.01, 0.01, 0.01)
        case .Gengar:
             pokemonModel.scale = SCNVector3(0.003, 0.003, 0.003)
        case .Flatty:
             pokemonModel.scale = SCNVector3(0.6, 0.6, 0.6)
        }
       
        //4. Position The Pokemon 1.5m Away From The Camera
        pokemonModel.position = SCNVector3(0, 0, -1.5)
        
        //5. Rotate The Pokemon So We Can It In All It's Glory
        //pokemonModel.rotate(.YAxis)
        
        getSizeOfModel(pokemonModel)
        
        print(pokemonModel.pivot)
        
        getOriginalAndScaledSizeOfNode(pokemonModel, scalar: 0.01)
        getSizeOfNodeAndPositionBubble(pokemonModel, scalar:0.003)
    }
    
    
    /// Returns The Width & Height Of An SCNNode
    ///
    /// - Parameter node: SCNNode
    func getSizeOfModel(_ node: SCNNode){
        
        //1. Get The Size Of The Node Without Scale
        let (minVec, maxVec) = node.boundingBox
        let unScaledHeight = maxVec.y - minVec.y
        let unScaledWidth = maxVec.x - minVec.x
        
        print("""
            UnScaled Height = \(unScaledHeight)
            UnScaled Width = \(unScaledWidth)
            """)
    }
    
    
    /// Returns The Original & Scaled With & Height On An SCNNode
    ///
    /// - Parameters:
    ///   - node: SCNode
    ///   - scalar: Float
    func getOriginalAndScaledSizeOfNode(_ node: SCNNode, scalar: Float){
        
        //1. Get The Size Of The Node Without Scale
        let (minVec, maxVec) = node.boundingBox
        let unScaledHeight = maxVec.y - minVec.y
        let unScaledWidth = maxVec.x - minVec.x
        
        print("""
            UnScaled Height = \(unScaledHeight)
            UnScaled Width = \(unScaledWidth)
            """)
        
        
        //2. Get The Size Of The Node With Scale
        let max = node.boundingBox.max
        let maxScale = SCNVector3(max.x * scalar, max.y * scalar, max.z * scalar)
        
        let min = node.boundingBox.min
        let minScale = SCNVector3(min.x * scalar, min.y * scalar, min.z * scalar)
        
        let heightOfNodeScaled = maxScale.y - minScale.y
        let widthOfNodeScaled = maxScale.x - minScale.x
        
        print("""
            Scaled Height = \(heightOfNodeScaled)
            Scaled Width = \(widthOfNodeScaled)
            """)
        
    }
    
    func getSizeOfNodeAndPositionBubble(_ node: SCNNode, scalar: Float){
        
        //1. Get The Size Of The Node Without Scale
        let (minVec, maxVec) = node.boundingBox
        let unScaledHeight = maxVec.y - minVec.y
        let unScaledWidth = maxVec.x - minVec.x
        
        print("""
        UnScaled Height = \(unScaledHeight)
        UnScaled Width = \(unScaledWidth)
        """)
     
     
        //2. Get The Size Of The Node With Scale
        let max = node.boundingBox.max
        let maxScale = SCNVector3(max.x * scalar, max.y * scalar, max.z * scalar)
        
        let min = node.boundingBox.min
        let minScale = SCNVector3(min.x * scalar, min.y * scalar, min.z * scalar)
        
        let heightOfNodeScaled = maxScale.y - minScale.y
        let widthOfNodeScaled = maxScale.x - minScale.x
       
        print("""
            Scaled Height = \(heightOfNodeScaled)
            Scaled Width = \(widthOfNodeScaled)
            """)
        
        //3. Create A Buubble
        let pointNodeHolder = SCNNode()
        let pointGeometry = SCNSphere(radius: 0.04)
        pointGeometry.firstMaterial?.diffuse.contents = UIColor.red
        pointNodeHolder.geometry = pointGeometry
        
        //4. Place The Bubble At The Origin Of The Model, At The Models Origin + It's Height & At The Z Position
        pointNodeHolder.position = SCNVector3(node.position.x, node.position.y + heightOfNodeScaled + 0.1, node.position.z)
        self.augmentedRealityView.scene.rootNode.addChildNode(pointNodeHolder)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        augmentedRealitySession.pause()
        augmentedRealitySession.run(configuration, options: runOptions(.ResetAndRemove))
    
        
       // 47.8663
       // 34.0906
    }
    //---------------
    //MARK: ARSession
    //---------------
    
    /// Sets Up The ARSession
    func setupARSession(){
        
        //1. Set The AR Session
        augmentedRealityView.session = augmentedRealitySession
        augmentedRealityView.delegate = self
        
        //2. Conifgure The Type Of Plane Detection
        configuration.planeDetection = [planeDetection(.Both)]
        
        //3. Configure The Debug Options
        augmentedRealityView.debugOptions = debug(.FeaturePoints)
        
        //4. If In Debug Mode Show Statistics
        #if DEBUG
        augmentedRealityView.showsStatistics = true
        #endif
        
        //5. Set The Scene Lighting
        augmentedRealityView.applyLighting()
        
        //6. Run The Session
        augmentedRealitySession.run(configuration, options: runOptions(.ResetAndRemove))
        
    }
    
}
